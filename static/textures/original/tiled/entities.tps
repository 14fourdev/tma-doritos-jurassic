<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Users/bryan/git/tma-doritos-jurassic/static/textures/original/tiled/entities.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../entities{n}.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../decorations/decoration-dino-apatosaurus.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>226,116,452,233</rect>
                <key>scale9Paddings</key>
                <rect>226,116,452,233</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-dino-baryonyx.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>92,65,184,131</rect>
                <key>scale9Paddings</key>
                <rect>92,65,184,131</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-dino-brachiosaurus.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>232,270,464,540</rect>
                <key>scale9Paddings</key>
                <rect>232,270,464,540</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-dino-carnotaurus.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,82,200,165</rect>
                <key>scale9Paddings</key>
                <rect>100,82,200,165</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-dino-gallimimus.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,58,106,116</rect>
                <key>scale9Paddings</key>
                <rect>53,58,106,116</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-dino-sinoceratops.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>97,78,195,157</rect>
                <key>scale9Paddings</key>
                <rect>97,78,195,157</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-dino-trex.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>104,101,209,203</rect>
                <key>scale9Paddings</key>
                <rect>104,101,209,203</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-dino-triceratops.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>117,83,233,166</rect>
                <key>scale9Paddings</key>
                <rect>117,83,233,166</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-distantfern1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,31,66,63</rect>
                <key>scale9Paddings</key>
                <rect>33,31,66,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-distantfern2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,36,78,73</rect>
                <key>scale9Paddings</key>
                <rect>39,36,78,73</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-foliageclump1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>198,99,397,198</rect>
                <key>scale9Paddings</key>
                <rect>198,99,397,198</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-foliageclump2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>224,90,449,179</rect>
                <key>scale9Paddings</key>
                <rect>224,90,449,179</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-foliageclump3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>189,87,377,173</rect>
                <key>scale9Paddings</key>
                <rect>189,87,377,173</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-foliageclump4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>170,94,340,187</rect>
                <key>scale9Paddings</key>
                <rect>170,94,340,187</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-foliageclump5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>305,92,610,184</rect>
                <key>scale9Paddings</key>
                <rect>305,92,610,184</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-tree1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,83,96,165</rect>
                <key>scale9Paddings</key>
                <rect>48,83,96,165</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-tree2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,86,145,171</rect>
                <key>scale9Paddings</key>
                <rect>73,86,145,171</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-tree3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,71,127,142</rect>
                <key>scale9Paddings</key>
                <rect>64,71,127,142</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-tree4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,57,58,114</rect>
                <key>scale9Paddings</key>
                <rect>29,57,58,114</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-tree5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>93,72,187,144</rect>
                <key>scale9Paddings</key>
                <rect>93,72,187,144</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-tree6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,98,63,196</rect>
                <key>scale9Paddings</key>
                <rect>32,98,63,196</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-tree7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,69,104,137</rect>
                <key>scale9Paddings</key>
                <rect>52,69,104,137</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/decoration-tree8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,71,89,141</rect>
                <key>scale9Paddings</key>
                <rect>45,71,89,141</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/envmap.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>67,64,133,128</rect>
                <key>scale9Paddings</key>
                <rect>67,64,133,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/gyrocrack1.png</key>
            <key type="filename">../entities/gyrocrack2.png</key>
            <key type="filename">../entities/gyrocrack3.png</key>
            <key type="filename">../entities/gyronospin.png</key>
            <key type="filename">../entities/gyrospin.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>60,60,120,120</rect>
                <key>scale9Paddings</key>
                <rect>60,60,120,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/gyroshadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,9,118,17</rect>
                <key>scale9Paddings</key>
                <rect>59,9,118,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-dinobones.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,49,169,98</rect>
                <key>scale9Paddings</key>
                <rect>84,49,169,98</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-impactcrater.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>254,40,509,79</rect>
                <key>scale9Paddings</key>
                <rect>254,40,509,79</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-lavapit.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>141,22,283,44</rect>
                <key>scale9Paddings</key>
                <rect>141,22,283,44</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-lavapit2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>122,21,245,43</rect>
                <key>scale9Paddings</key>
                <rect>122,21,245,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-lavapit3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>91,16,182,32</rect>
                <key>scale9Paddings</key>
                <rect>91,16,182,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-lavapit4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,22,227,43</rect>
                <key>scale9Paddings</key>
                <rect>114,22,227,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-oldgyro.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>125,80,249,160</rect>
                <key>scale9Paddings</key>
                <rect>125,80,249,160</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-ramp.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>116,54,231,109</rect>
                <key>scale9Paddings</key>
                <rect>116,54,231,109</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-rocks.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.262195,0.751037</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>185,109,370,219</rect>
                <key>scale9Paddings</key>
                <rect>185,109,370,219</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-rocks2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>109,45,218,90</rect>
                <key>scale9Paddings</key>
                <rect>109,45,218,90</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-rocks3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>101,51,202,101</rect>
                <key>scale9Paddings</key>
                <rect>101,51,202,101</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-rocks4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>124,42,247,84</rect>
                <key>scale9Paddings</key>
                <rect>124,42,247,84</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/obstacle-rocks5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>91,52,181,105</rect>
                <key>scale9Paddings</key>
                <rect>91,52,181,105</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/powerup_boost.png</key>
            <key type="filename">../entities/powerup_boost_0000.png</key>
            <key type="filename">../entities/powerup_boost_0001.png</key>
            <key type="filename">../entities/powerup_boost_0002.png</key>
            <key type="filename">../entities/powerup_boost_0003.png</key>
            <key type="filename">../entities/powerup_boost_0004.png</key>
            <key type="filename">../entities/powerup_boost_0005.png</key>
            <key type="filename">../entities/powerup_boost_0006.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,38,67,75</rect>
                <key>scale9Paddings</key>
                <rect>33,38,67,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/powerup_repair.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,32,70,64</rect>
                <key>scale9Paddings</key>
                <rect>35,32,70,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/powerup_repair_0000.png</key>
            <key type="filename">../entities/powerup_repair_0001.png</key>
            <key type="filename">../entities/powerup_repair_0002.png</key>
            <key type="filename">../entities/powerup_repair_0003.png</key>
            <key type="filename">../entities/powerup_repair_0004.png</key>
            <key type="filename">../entities/powerup_repair_0005.png</key>
            <key type="filename">../entities/powerup_repair_0006.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,63,125,125</rect>
                <key>scale9Paddings</key>
                <rect>63,63,125,125</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../entities/powerup_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,7,40,15</rect>
                <key>scale9Paddings</key>
                <rect>20,7,40,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../entities/gyronospin.png</filename>
            <filename>../entities/gyroshadow.png</filename>
            <filename>../entities/gyrospin.png</filename>
            <filename>../entities/powerup_boost.png</filename>
            <filename>../entities/powerup_repair.png</filename>
            <filename>../entities/powerup_shadow.png</filename>
            <filename>../entities/gyrocrack1.png</filename>
            <filename>../entities/gyrocrack2.png</filename>
            <filename>../entities/gyrocrack3.png</filename>
            <filename>../entities/obstacle-dinobones.png</filename>
            <filename>../entities/obstacle-impactcrater.png</filename>
            <filename>../entities/obstacle-lavapit.png</filename>
            <filename>../entities/obstacle-oldgyro.png</filename>
            <filename>../entities/obstacle-ramp.png</filename>
            <filename>../entities/obstacle-rocks.png</filename>
            <filename>../decorations/decoration-distantfern1.png</filename>
            <filename>../decorations/decoration-distantfern2.png</filename>
            <filename>../decorations/decoration-foliageclump1.png</filename>
            <filename>../decorations/decoration-foliageclump2.png</filename>
            <filename>../decorations/decoration-foliageclump3.png</filename>
            <filename>../decorations/decoration-foliageclump4.png</filename>
            <filename>../decorations/decoration-foliageclump5.png</filename>
            <filename>../decorations/decoration-tree1.png</filename>
            <filename>../decorations/decoration-tree2.png</filename>
            <filename>../decorations/decoration-tree3.png</filename>
            <filename>../decorations/decoration-tree4.png</filename>
            <filename>../decorations/decoration-tree5.png</filename>
            <filename>../decorations/decoration-tree6.png</filename>
            <filename>../decorations/decoration-tree7.png</filename>
            <filename>../decorations/decoration-tree8.png</filename>
            <filename>../entities/powerup_boost_0000.png</filename>
            <filename>../entities/powerup_boost_0001.png</filename>
            <filename>../entities/powerup_boost_0002.png</filename>
            <filename>../entities/powerup_boost_0003.png</filename>
            <filename>../entities/powerup_boost_0004.png</filename>
            <filename>../entities/powerup_boost_0005.png</filename>
            <filename>../entities/powerup_boost_0006.png</filename>
            <filename>../entities/powerup_repair_0000.png</filename>
            <filename>../entities/powerup_repair_0001.png</filename>
            <filename>../entities/powerup_repair_0002.png</filename>
            <filename>../entities/powerup_repair_0003.png</filename>
            <filename>../entities/powerup_repair_0004.png</filename>
            <filename>../entities/powerup_repair_0005.png</filename>
            <filename>../entities/powerup_repair_0006.png</filename>
            <filename>../entities/obstacle-rocks2.png</filename>
            <filename>../entities/obstacle-rocks3.png</filename>
            <filename>../entities/obstacle-rocks4.png</filename>
            <filename>../entities/obstacle-rocks5.png</filename>
            <filename>../entities/obstacle-lavapit2.png</filename>
            <filename>../entities/obstacle-lavapit3.png</filename>
            <filename>../entities/obstacle-lavapit4.png</filename>
            <filename>../entities/envmap.png</filename>
            <filename>../decorations/decoration-dino-apatosaurus.png</filename>
            <filename>../decorations/decoration-dino-baryonyx.png</filename>
            <filename>../decorations/decoration-dino-brachiosaurus.png</filename>
            <filename>../decorations/decoration-dino-carnotaurus.png</filename>
            <filename>../decorations/decoration-dino-gallimimus.png</filename>
            <filename>../decorations/decoration-dino-sinoceratops.png</filename>
            <filename>../decorations/decoration-dino-trex.png</filename>
            <filename>../decorations/decoration-dino-triceratops.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
