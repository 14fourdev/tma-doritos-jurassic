import 'promise-polyfill/src/polyfill'
import Vue from 'vue'
import App from './App'
import store from './store'

window.isIE11 = !!window.MSInputMethodContext && !!document.documentMode
Vue.config.productionTip = false
/* eslint-disable no-new */

new Vue({
  el: '#app',
  store,
  template: '<App/>',
  components: { App }
})
