import * as PIXI from 'pixi.js'
import Global from '@/scripts/game/Global'
import World from '@/scripts/game/World'
import EventBus from '@/scripts/game/EventBus'
import InputManager from '@/scripts/game/InputManager'
import ParallaxBackgroundConfig from '@/scripts/game/ParallaxBackgroundConfig'
import ItemPickupManager from '@/scripts/game/ItemPickupManager'
import ParticleManager from '@/scripts/game/ParticleManager'
import AudioManager from '@/scripts/game/AudioManager'
import VignetteManager from '@/scripts/game/VignetteManager'
import AnimationManager from '@/scripts/game/AnimationManager'
import EntityManager from '@/scripts/game/EntityManager'
import DecorationManager from '@/scripts/game/DecorationManager'
import PanZoom from '@/scripts/game/PanZoom'

class JurassicGame {
  constructor (component) {
    window.global = Global
    Global.game = this
    Global.component = this.component = component
    Global.canvas = component.$refs.canvas
    Global.canvas.addEventListener('contextmenu', (e) => {
      e.preventDefault()
    })
    this.__initDefaults()
    this.__init()
  }

  __init () {
    this.__initSounds()
    this.__initPixi()
    this.__initCamera()
    this.__initWorld().then(() => {
      this.__initEditor()
      this.__initEntities()
      this.__initListeners()
      this.__initVignette()
      this.__start()
    })
  }

  __initVignette () {
    VignetteManager.init()
  }

  __startPause () {
    AudioManager.mute(true)
    this.paused = true
    this.component.paused = true
    this.component.howToPlay = true
    Global.camera.pivot.x = Global.screen.width / 2
    Global.camera.pivot.y = EntityManager.player.sprite.y
  }

  unpause () {
    if (this.initialUpdate) {
      this.initialUpdate = false
      this.__playStartAudio()
    }
    this.togglePause(false)
  }

  __start () {
    this.component.ready = true
    console.log('ready')
    this.component.$refs.modal.setLoading(false)
    Global.app.ticker.add(this.__update.bind(this))
  }

  __playStartAudio () {
    AudioManager.play('jw-stinger-intro')
    AudioManager.play('music-background')
    AudioManager.play('stampede')
    AudioManager.sounds['stampede'].audio.volume(0)
  }

  __initSounds () {
    AudioManager.load('general')
  }

  __initDefaults () {
    this.dy = 0
    this.boost = 100
    this.health = 100
    this.accum = 0
    this.counter = 0
    this.playerProgress = 0
    this.dinoProgress = Math.min(1, (this.counter - 300) / (60 * 60))
    this.paused = false
    this.shakeMultiplier = 0
    this.altShakeMultiplier = 0
    this.altShakeCounter = 0
    this.altShakeMaxDuration = 1
    this.initialUpdate = true
  }

  __initPixi () {
    Global.app = new PIXI.Application({
      view: Global.canvas,
      width: 1280,
      height: 720
    })
    Global.screen = Global.app.screen
    Global.renderer = Global.app.renderer
  }

  __initCamera () {
    Global.camera = new PIXI.Container()
    Global.camera.position.set(Global.app.screen.width / 2, Global.app.screen.height / 2)
    Global.app.stage.addChild(Global.camera)
  }

  __initWorld () {
    return new Promise((resolve, reject) => {
      this.layers = {
        background: new PIXI.Container(),
        decoration: DecorationManager.container,
        level: new PIXI.Container(),
        entity: EntityManager.container,
        foreground: new PIXI.Container(),
        vignette: VignetteManager.container
      }
      ParticleManager.init()
      Global.camera.addChild(this.layers.background)
      Global.camera.addChild(this.layers.decoration)
      Global.camera.addChild(this.layers.level)
      Global.camera.addChild(this.layers.entity)
      Global.camera.addChild(this.layers.foreground)
      Global.camera.addChild(this.layers.vignette)
      Global.world = new World()
      Global.world.container.forEach(container => this.layers.level.addChild(container))
      Global.world.load('static/levels/level1.json')
        .then(() => {
          ParallaxBackgroundConfig
            .filter(parallaxCfg => !parallaxCfg.foreground)
            .sort((a, b) => a.z - b.z)
            .forEach(cfg => {
              this.layers.background.addChild(Global.world.parallax[cfg.key].sprite)
            })
          ParallaxBackgroundConfig
            .filter(parallaxCfg => parallaxCfg.foreground)
            .sort((a, b) => a.z - b.z)
            .forEach(cfg => { this.layers.foreground.addChild(Global.world.parallax[cfg.key].sprite) })
          resolve()
        })
        .catch(reject)
    })
  }

  __initEntities () {
    EntityManager.init()
  }

  __initEditor () {
    if (process.env.NODE_ENV === 'development') {
      import('@/scripts/game/Editor').then(module => {
        Global.editor = new module.Editor(this)
      })
    }
  }

  __initListeners () {
    EventBus.hookListeners()
    EventBus.on('keydown', this.__onKeyDown.bind(this))
    EventBus.on('keyup', this.__onKeyUp.bind(this))
    PanZoom(document, (e) => {
      if (!this.paused) {
        if (e.dy > 5 || e.dz > 5) {
          InputManager.swipeDown = true
        }
        if (e.dy < -5 || e.dz < -5) {
          InputManager.swipeUp = true
        }
      }
    })
  }

  __onKeyDown (e) {
    if (e.keyCode === 38 || e.keyCode === 87) {
      InputManager.swipeUp = true
    }
    if (e.keyCode === 40 || e.keyCode === 83) {
      InputManager.swipeDown = true
    }
    if ((e.keyCode === 68 || e.keyCode === 39) && !e.repeat) {
      InputManager.boosting = true
      EntityManager.player.breakBoost = false
    }
    if ((e.keyCode === 65 || e.keyCode === 37) && !e.repeat) {
      InputManager.braking = true
    }
  }

  __onKeyUp (e) {
    if (e.keyCode === 68 || e.keyCode === 39) {
      InputManager.boosting = false
    }
    if (e.keyCode === 65 || e.keyCode === 37) {
      InputManager.braking = false
    }
  }

  __update (delta) {
    if (!this.paused) {
      if (delta) {
        this.accum += delta
      }
      while (this.accum >= 1) {
        this.accum -= 1
        this.__updateProgress(delta)
        this.__checkInputs(delta)
        this.__updateCamera(delta)
        if (process.env.NODE_ENV === 'development') {
          if (!this.paused) {
            this.__updateEntities(delta)
          }
        } else {
          this.__updateEntities(delta)
        }
        this.__updateItemPickups(delta)
        this.__updateParticles(delta)
        this.__updateAnimations(delta)
      }
    }
    this.__updateWorld(delta)
    this.__updateComponent(delta)
    this.__cullEntities()
    this.__cullDecorations()
    this.__updateVignette(delta)
    if (process.env.NODE_ENV === 'development') {
      if (Global.editor && Global.editor.enabled) {
        Global.editor.__update(delta)
      }
    }
    this.__sortEntities()

    if (this.initialUpdate) {
      this.__startPause()
    }
  }

  __updateVignette (delta) {
    VignetteManager.update(delta)
  }

  __updateAnimations (delta) {
    AnimationManager.update(delta)
  }

  __cullEntities () {
    EntityManager.cull()
  }

  __cullDecorations () {
    DecorationManager.cull()
  }

  __updateItemPickups (delta) {
    ItemPickupManager.update()
  }

  __updateParticles (delta) {
    ParticleManager.update()
  }

  __updateEntities (delta) {
    EntityManager.update()
  }

  __sortEntities () {
    EntityManager.sort()
  }

  __updateProgress () {
    this.counter++
    if (EntityManager.player) {
      this.playerProgress = Math.min(1, EntityManager.player.sprite.x / Global.world.data.width)
      this.dinoProgress = Math.min(1, (this.counter - 300) / (60 * 60))
      this.shakeMultiplier = Math.min(1, Math.max(0, 1 - (this.playerProgress - this.dinoProgress) * 30))
      if (!window.isIE11) {
        AudioManager.sounds['stampede'].audio.volume(this.shakeMultiplier)
      }
    }
    if (this.playerProgress >= 1) {
      this.__win()
    }
    if (this.dinoProgress > this.playerProgress) {
      this.__lose()
    }
  }

  __updateWorld (delta) {
    if (Global.world) {
      Global.world.update()
    }
  }

  __updateComponent (delta) {
    this.component.health = this.health
    this.component.boost = this.boost
    this.component.playerProgress = this.playerProgress
    this.component.dinoProgress = this.dinoProgress
  }

  __updateCamera (delta) {
    if (EntityManager.player && !(Global.editor && Global.editor.enabled)) {
      if (!this.interpVX) {
        this.interpVX = EntityManager.player.velocity[0] * 30
      }
      this.interpVX = (this.interpVX * 14 + EntityManager.player.velocity[0] * 30) / 15
      this.dy = (this.dy * 19 + EntityManager.player.dy) / 20
      Global.camera.pivot.x = Math.min(Math.max(EntityManager.player.sprite.x + this.interpVX + this.__getStampedeShake() + this.__getAltShake(), Global.screen.width / 2), Global.world.data.width - Global.screen.width / 2)
      Global.camera.pivot.y = Math.min(800, EntityManager.player.sprite.y + this.dy * 10 - Global.screen.height / 12 + this.__getStampedeShake() + this.__getAltShake())
      if (this.altShakeCounter > 0) {
        this.altShakeCounter -= 1
      }
    }
  }

  __getStampedeShake () {
    return Math.random() * this.shakeMultiplier * 20 - this.shakeMultiplier * 10
  }

  __getAltShake () {
    return (Math.random() * this.altShakeMultiplier - this.altShakeMultiplier / 2) * (this.altShakeCounter / this.altShakeMaxDuration)
  }

  __checkInputs () {
    if (EntityManager.player) {
      this.braking = InputManager.braking
      if (EntityManager.player.boosting) {
        this.boost -= 0.5
      } else {
        this.boost += 0.25
        this.boost = Math.min(this.boost, 100)
      }
      if (InputManager.swipeDown) {
        EntityManager.player.changeLane(1)
        InputManager.swipeDown = false
      }
      if (InputManager.swipeUp) {
        EntityManager.player.changeLane(-1)
        InputManager.swipeUp = false
      }
    }
  }

  __win () {
    this.__stopSounds()
    AudioManager.play('jw-stinger-win')
    this.paused = true
    this.component.win()
    window.parent.postMessage('win', '*')
  }

  __lose () {
    this.__stopSounds()
    AudioManager.play('jw-stinger-lose')
    this.paused = true
    this.component.lose()
    window.parent.postMessage('lose', '*')
  }

  __stopSounds () {
    AudioManager.stopAll()
  }

  __restart () {
    this.__initDefaults()
    Global.world.reset()
    ItemPickupManager.reset()
    ParticleManager.reset()
    AnimationManager.reset()
    EntityManager.reset()
    AudioManager.stopAll()
    this.component.restart()
  }

  togglePause (force = null) {
    this.paused = force !== null ? force : !this.paused
    AudioManager.mute(this.paused)
    this.component.paused = this.paused
  }

  addHealth (health) {
    this.health = Math.min(100, this.health + health)
    if (this.health > 75) {
      EntityManager.player.setDamageTexture('gyrospin.png')
    } else if (this.health > 50) {
      EntityManager.player.setDamageTexture('gyrocrack1.png')
    } else if (this.health > 25) {
      EntityManager.player.setDamageTexture('gyrocrack2.png')
    } else {
      EntityManager.player.setDamageTexture('gyrocrack3.png')
    }
  }

  addBoost (boost) {
    this.boost = Math.min(100, this.boost + boost)
  }

  screenShake (x, power, duration = 60) {
    let multi = Math.max(0, power * 5 - Math.sqrt(Math.abs(x - EntityManager.player.sprite.x)))
    if (this.altShakeCounter <= 0 || multi > this.altShakeMultiplier) {
      this.altShakeMaxDuration = duration
      this.altShakeCounter = 60
      this.altShakeMultiplier = multi
    }
  }
}

export default JurassicGame
