import * as PIXI from 'pixi.js'
import MathJS from 'mathjs'
import Global from '@/scripts/game/Global'
import Easings from '@/scripts/game/Easings'
import TextureManager from '@/scripts/game/TextureManager'
import ItemPickupManager from '@/scripts/game/ItemPickupManager'
import AudioManager from '@/scripts/game/AudioManager'
import ParticleManager from '@/scripts/game/ParticleManager'
import GyroDust from '@/scripts/game/particles/GyroDust'
import InputManager from '@/scripts/game/InputManager'
import CircleMap from '@/scripts/game/shaders/CircleMap.frag'

class Gyrosphere {
  constructor () {
    this.sprite = new PIXI.Container()
    this.sprite.addChild(new PIXI.Sprite(TextureManager.sprites['gyronospin.png']))
    this.spinSprite = new PIXI.Sprite(TextureManager.sprites['gyrospin.png'])
    this.sprite.addChild(this.spinSprite)
    this.sprite.addChild(new PIXI.Sprite(TextureManager.sprites['gyroshadow.png']))
    if (Global.renderer instanceof PIXI.WebGLRenderer) {
      let filter = new PIXI.Filter('', CircleMap)
      filter.autoFit = false
      filter.apply = function (filterManager, input, output, clear) {
        this.uniforms.dimensions[0] = input.sourceFrame.width
        this.uniforms.dimensions[1] = input.sourceFrame.height
        filterManager.applyFilter(this, input, output, clear)
      }
      this.mapSprite = new PIXI.extras.TilingSprite(TextureManager.sprites['envmap.png'], 240, 240)
      this.alphaFilter = new PIXI.filters.AlphaFilter(0.8)
      this.alphaFilter.blendMode = PIXI.BLEND_MODES.SCREEN
      this.mapSprite.filters = [ filter, this.alphaFilter ]
      this.sprite.addChild(this.mapSprite)
    }
    this.sprite.customData = {}
    this.potentialObstacles = []
    this.initDefaults()
  }

  initDefaults () {
    this.sprite.x = 120
    this.sprite.y = 99
    this.sprite.children[0].anchor.set(0.5, 0.5)
    this.sprite.children[0].scale.set(0.65)
    this.sprite.children[1].anchor.set(0.5, 0.5)
    this.sprite.children[1].scale.set(0.65)
    this.sprite.children[2].anchor.set(0.5, -3)
    this.sprite.children[2].scale.set(0.65)
    if (this.mapSprite) {
      this.sprite.children[3].anchor.set(0.5, 0.5)
      this.sprite.children[3].scale.set(0.65)
    }
    this.sprite.children[0].alpha = 1
    this.sprite.children[1].alpha = 1
    this.sprite.children[2].alpha = 1
    this.radius = 64
    this.gravity = [0, 0.1]
    this.velocity = [12, 0]
    this.boost = 0
    this.brake = 1
    this.dy = 0
    this.lastLane = 0
    this.friction = 1.03
    this.laneTransitionTime = 30
    this.transitioningLane = 0
    this.blinkTimer = 0
    this.hitRamp = false
    this.firstUpdate = 0
    this.lastHit = null
    this.lastSpecial = null
    this.counter = 0
    this.setDamageTexture('gyrospin.png')
    this.__setLane(1)
  }

  __update () {
    if (this.mapSprite) {
      this.mapSprite.tilePosition.x = -(this.sprite.x % this.mapSprite.texture.orig.width)
    }
    if (this.firstUpdate < 2) {
      if (this.firstUpdate === 1) {
        AudioManager.play('gyrosphere-movement')
        AudioManager.play('gyrosphere-gravel')
        AudioManager.play('gyrosphere-grass')
      }
      this.firstUpdate += 1
    }
    if (InputManager.boosting && !this.hitRamp && !this.breakBoost) {
      if (Global.game.boost >= 0.5) {
        this.boosting = true
      } else {
        this.breakBoost = true
      }
    } else {
      this.boosting = false
    }
    if (this.hitRamp) {
      this.__onRampUpdate()
    } else {
      this.__defaultUpdate()
    }
    this.__checkBlink()
    this.sprite.children[1].rotation += this.velocity[0] / 110
    AudioManager.sounds['gyrosphere-movement'].audio.rate(this.velocity[0] / 10 + 0.5)
    AudioManager.sounds['gyrosphere-grass'].audio.rate(this.velocity[0] / 10 + 0.5)
    AudioManager.sounds['gyrosphere-gravel'].audio.rate(this.velocity[0] / 10 + 0.5)
    this.counter++
  }

  __onRampUpdate () {
    let distance = this.sprite.x - this.hitRamp.x + 180
    if (distance < 25 * this.lane + 240) {
      let ty = this.hitRamp.y - (30 * this.lane + 70) - Math.sin(Math.PI / 7 - this.hitRamp.rotation) * (distance)
      this.sprite.x += this.velocity[0]
      this.sprite.y = (this.sprite.y * 7 + ty) / 8
      this.__createDustParticles()
    } else {
      if (!this.inAir) {
        this.inAir = true
        this.__fadeOutSounds()
      }
      this.sprite.x += this.velocity[0]
      this.sprite.y += this.velocity[1]
      this.velocity[1] += 0.4
      let y = Global.world.getFloorYOfSegment(this.sprite.x, Global.world.getStartEndPointsOfPlatformLineSegment(this.sprite.x, this.lane)) - this.radius
      if (this.sprite.y > y) {
        Global.game.screenShake(this.sprite.x, 0.1, 5)
        this.inAir = false
        this.hitRamp = false
        AudioManager.play('gyrosphere-jump-landing')
      }
    }
  }

  __defaultUpdate () {
    this.oldVx = this.velocity[0]
    this.dy = this.sprite.y
    this.boost = this.boosting ? Math.min(0.5, this.boost + 0.05) : Math.max(0.2, this.boost - 0.05)
    this.brake = Global.game.braking ? Math.max(0.5, this.brake - 0.1) : Math.min(1, this.brake + 0.1)
    let points = null
    if (this.transitioningLane > 0) {
      let t = Easings.easeInOutQuad((this.laneTransitionTime - this.transitioningLane) / this.laneTransitionTime)
      this.__mixGrassGravelSound(this.targetLane === 1 ? t : (1 - t))
      this.sprite.children[0].scale.set(0.5 + (this.sourceLane * 0.15 * (1 - t)) + (this.targetLane * 0.15 * t))
      this.sprite.children[1].scale.set(0.5 + (this.sourceLane * 0.15 * (1 - t)) + (this.targetLane * 0.15 * t))
      this.sprite.children[2].scale.set(0.5 + (this.sourceLane * 0.15 * (1 - t)) + (this.targetLane * 0.15 * t))
      if (this.mapSprite) {
        this.sprite.children[3].scale.set(0.5 + (this.sourceLane * 0.15 * (1 - t)) + (this.targetLane * 0.15 * t))
      }
      if (this.sourceLane > this.targetLane) {
        points = Global.world.getStartEndPointsOfPlatformLineSegment(this.sprite.x, this.sourceLane, this.targetLane, 1 - t)
      } else {
        points = Global.world.getStartEndPointsOfPlatformLineSegment(this.sprite.x, this.sourceLane, this.targetLane, t)
      }
      this.transitioningLane -= 1
      if (t > 0.8) {
        this.lastLane = this.lane
        this.__setPotentialObstacles(this.targetLane)
        this.lane = this.targetLane
      } else if (t > 0.2) {
        this.__setPotentialObstacles(null)
      } else {
        this.__setPotentialObstacles(this.sourceLane)
      }
      if (this.transitioningLane === 0) {
        this.__setLane(this.targetLane)
      }
    } else {
      this.__mixGrassGravelSound(this.lane === 1 ? 1 : 0)
      points = Global.world.getStartEndPointsOfPlatformLineSegment(this.sprite.x, this.lane)
    }
    if (points) {
      let tan = this.__getTangentOfSegment(this.sprite.x, points)
      this.sprite.y = Global.world.getFloorYOfSegment(this.sprite.x, points) - this.radius
      this.velocity[0] = (this.velocity[0] - (tan[0] - this.boost) * this.brake) / this.friction
      this.sprite.x += this.velocity[0]
      let targetRotation = (this.oldVx - this.velocity[0]) * 3
      this.sprite.children[0].rotation = (this.sprite.children[0].rotation * 19 + targetRotation) / 20
    }
    this.dy = this.sprite.y - this.dy
    this.__checkCollisions()
    this.__createDustParticles()
  }

  __mixGrassGravelSound (t) {
    if (!window.isIE11) {
      AudioManager.sounds['gyrosphere-gravel'].audio.volume(t * 0.6)
      AudioManager.sounds['gyrosphere-grass'].audio.volume((1 - t) * 0.6)
    }
  }

  __fadeOutSounds () {
    AudioManager.sounds['gyrosphere-gravel'].audio.fade(AudioManager.sounds['gyrosphere-gravel'].audio.volume(), 0, 200)
    AudioManager.sounds['gyrosphere-grass'].audio.fade(AudioManager.sounds['gyrosphere-grass'].audio.volume(), 0, 200)
  }

  __createDustParticles () {
    let interval = Math.floor(Math.max(1, 10 - this.velocity[0]))
    if (this.counter % interval === 0) {
      let particle = new GyroDust(this.sprite.x, this.sprite.y, this.lane, Math.min(1, this.velocity[0] / 20))
      ParticleManager.addParticle(particle)
    }
  }

  __checkBlink () {
    if (this.blinkTimer > 0) {
      this.blinkTimer -= 1
      this.sprite.children[0].alpha =
      this.sprite.children[1].alpha =
      this.sprite.children[2].alpha = (Math.floor(this.blinkTimer / 2) % 2 === 0) ? 1 : 0.35
      // if (this.blinkTimer === 0) {
      //   // remove blink effect
      // }
    }
  }

  __checkCollisions () {
    this.potentialObstacles.forEach(obstacle => {
      let bounds = obstacle.__runtime.bounds
      if (bounds) {
        let spriteBounds = {
          x: obstacle.__runtime.graphic.x - obstacle.__runtime.graphic.anchor.x * obstacle.__runtime.graphic.width,
          width: obstacle.__runtime.graphic.width
        }
        spriteBounds.x += spriteBounds.width * bounds.x1
        spriteBounds.width = (bounds.x2 - bounds.x1) * spriteBounds.width
        if ((this.sprite.x + this.sprite.width / 2) > spriteBounds.x && (this.sprite.x - this.sprite.width / 2) < (spriteBounds.x + spriteBounds.width)) {
          this.__hitObstacle(obstacle)
        }
      } else {
        let distance = Math.abs(this.sprite.x - obstacle.x)
        if (distance < obstacle.__runtime.graphic.width / 3 + this.sprite.width / 4) {
          this.__hitObstacle(obstacle)
        }
      }
    })
  }

  __hitObstacle (obstacle) {
    if (obstacle.__runtime.special) {
      this.__hitSpecialObstacle(obstacle)
    } else {
      if (this.blinkTimer <= 0 && this.lastHit !== obstacle) {
        this.__hitGeneralObstacle(obstacle)
      }
    }
  }

  __hitGeneralObstacle (obstacle) {
    Global.game.screenShake(this.sprite.x, 0.35, 7)
    Global.game.addHealth(-15)
    this.lastHit = obstacle
    this.velocity[0] = 3
    this.blinkTimer = 60
    if (obstacle.layer === 1 && this.transitioningLane > 0) {
      this.changeLane(this.lastLane > this.lane ? 1 : -1, true)
    } else {
      this.changeLane(obstacle.layer === 1 ? (Math.random() < 0.5 ? -1 : 1) : (1 - obstacle.layer), true)
    }
    if (Global.game.health <= 0) {
      Global.game.health = 0
      Global.game.__lose()
    }
    this.__hitCollisionSound(obstacle.key)
  }

  __hitSpecialObstacle (obstacle) {
    if (this.lastSpecial !== obstacle) {
      this.lastSpecial = obstacle
      if (obstacle.__runtime.special === 'boost' || obstacle.__runtime.special === 'repair') {
        ItemPickupManager.collectObstacle(obstacle)
      }
      if (obstacle.__runtime.special === 'ramp') {
        this.hitRamp = obstacle
        // if (this.transitioningLane && this.targetLane === obstacle.layer) {
        // }
        this.velocity[1] = this.velocity[0] * -Math.sin(Math.PI / 7 - this.hitRamp.rotation)
      }
      this.__hitCollisionSound(obstacle.__runtime.special)
    }
  }

  __hitCollisionSound (obstacleKey) {
    switch (obstacleKey) {
      case 'oldgyro':
        AudioManager.play('obstacle-hit-gyrosphere')
        break
      case 'dinobones':
        AudioManager.play('obstacle-hit-bones')
        break
      case 'lavapit':
      case 'lavapit2':
      case 'lavapit3':
      case 'lavapit4':
        AudioManager.play('obstacle-hit-lava')
        break
      case 'impactcrater':
        AudioManager.play('obstacle-hit-crater')
        break
      case 'rocks':
      case 'rocks2':
      case 'rocks3':
      case 'rocks4':
      case 'rocks5':
        AudioManager.play('obstacle-hit-rock')
        break
      case 'boost':
        AudioManager.play('powerup-boost')
        break
      case 'repair':
        AudioManager.play('powerup-repair')
        break
      case 'ramp':
        break
    }
  }

  __setPotentialObstacles (lane, force = false) {
    if (lane !== null) {
      if (this.potentialObstaclesLane !== lane || force) {
        this.potentialObstaclesLane = lane
        this.potentialObstacles = Global.world.data.obstacles.filter(obstacle => obstacle.__runtime.type === 'obstacle' && obstacle.layer === lane)
      }
    } else if (this.potentialObstaclesLane !== null) {
      this.potentialObstacles = []
      this.potentialObstaclesLane = null
    }
  }

  __setLane (lane) {
    this.lane = lane
    this.sprite.customData.layer = this.lane + 0.1
    this.__setPotentialObstacles(lane)
    // Global.game.__sortEntities()
  }

  __getTangentOfSegment (x, points) {
    let theta = Math.atan2(points[1][1] - points[0][1], points[1][0] - points[0][0])
    let n = [Math.cos(theta), Math.sin(theta)]
    return MathJS.subtract(this.gravity, MathJS.multiply(MathJS.dot(this.gravity, n), n))
  }

  changeLane (direction, force) {
    if (this.transitioningLane <= 0 || force) {
      if (this.lane < 2 && direction === 1) {
        this.transitionToLane(this.lane + 1)
        this.sprite.customData.layer = this.lane + 0.1
        // Global.game.__sortEntities()
      } else if (this.lane > 0 && direction === -1) {
        this.transitionToLane(this.lane - 1)
        this.sprite.customData.layer = this.lane - 0.1
        // Global.game.__sortEntities()
      }
    }
  }

  transitionToLane (lane, transitionTime = this.laneTransitionTime) {
    this.transitioningLane = transitionTime
    this.targetLane = lane
    this.sourceLane = this.lane
  }

  setDamageTexture (texture) {
    this.spinSprite.texture = TextureManager.sprites[texture]
  }
}

export default Gyrosphere
