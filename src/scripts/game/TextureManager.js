import Global from '@/scripts/game/Global'
import * as PIXI from 'pixi.js'

class TextureManager {

  buildTexturesFromWorld () {
    return new Promise((resolve) => {
      this.atlasImage = new Image()
      this.atlasImage.addEventListener('load', () => {
        if (!PIXI.loader.resources['world']) {
          PIXI.loader.add(Global.world.data.atlasKey, `static/textures/${Global.world.data.atlasKey}.json`)
          PIXI.loader.add('vignette', `static/textures/vignette.json`)
          PIXI.loader.add('entities0', `static/textures/entities0.json`)
          PIXI.loader.add('entities1', `static/textures/entities1.json`)
          PIXI.loader.add('entities2', `static/textures/entities2.json`)
          PIXI.loader.add('particles', `static/textures/particles.json`)
          PIXI.loader.add('background0', `static/textures/background0.json`)
          PIXI.loader.add('background1', `static/textures/background1.json`)
          PIXI.loader.add('background2', `static/textures/background2.json`)
          PIXI.loader.add('background3', `static/textures/background3.json`)
          PIXI.loader.load((loader, resources) => {
            this.sprites = PIXI.utils.TextureCache
            resolve()
          })
        }
      })
      this.atlasImage.src = `static/textures/${Global.world.data.atlasKey}.png`
    })
  }

  buildPlatform (platform) {
    if (platform.textures.length >= 3) {
      const leftTexture = this.sprites[platform.textures[0]]
      const centerTexture = this.sprites[platform.textures[1]]
      const rightTexture = this.sprites[platform.textures[2]]
      const canvas = document.createElement('canvas')
      const context = canvas.getContext('2d')
      canvas.width = platform.__runtime.curve.length
      canvas.height = centerTexture.height
      context.clearRect(0, 0, canvas.width, canvas.height)

      // draw start cap
      context.drawImage(
        this.atlasImage,
        leftTexture.orig.x,
        leftTexture.orig.y,
        leftTexture.orig.width,
        leftTexture.orig.height,
        0,
        0,
        leftTexture.orig.width,
        leftTexture.orig.height
      )

      // draw center portions as repeating tiles
      for (let i = leftTexture.width; i < canvas.width - rightTexture.width; i += centerTexture.width) {
        context.drawImage(this.atlasImage,
          centerTexture.orig.x,
          centerTexture.orig.y,
          centerTexture.orig.width,
          centerTexture.orig.height,
          i,
          0,
          centerTexture.orig.width,
          centerTexture.orig.height
        )
      }

      // draw end cap
      context.drawImage(this.atlasImage,
        rightTexture.orig.x,
        rightTexture.orig.y,
        rightTexture.orig.width,
        rightTexture.orig.height,
        canvas.width - rightTexture.orig.width,
        0,
        rightTexture.orig.width,
        rightTexture.orig.height
      )

      platform.__runtime.texture = PIXI.Texture.fromCanvas(canvas)
      // platform.__runtime.texture.update()
    } else {
      const centerTexture = this.sprites[platform.textures[0]]
      const canvas = document.createElement('canvas')
      const context = canvas.getContext('2d')
      canvas.width = platform.__runtime.curve.length
      canvas.height = centerTexture.height
      context.clearRect(0, 0, canvas.width, canvas.height)

      // draw center portions as repeating tiles
      for (let i = 0; i < canvas.width; i += centerTexture.width) {
        context.drawImage(this.atlasImage,
          centerTexture.orig.x,
          centerTexture.orig.y,
          centerTexture.orig.width,
          centerTexture.orig.height,
          i,
          0,
          centerTexture.orig.width,
          centerTexture.orig.height
        )
      }

      platform.__runtime.texture = PIXI.Texture.fromCanvas(canvas)
    }
  }
}

export default new TextureManager()
