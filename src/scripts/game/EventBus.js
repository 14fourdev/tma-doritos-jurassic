import Global from '@/scripts/game/Global'
import Event from '@/scripts/game/Event'
import Util from '@/scripts/game/Util'

class EventBus {
  constructor () {
    this.events = {}
  }

  __hookBasicEvents () {
    this.basicEvents.forEach((event) => {
      event.listeners = {}
      event.events.forEach((name) => {
        event.listeners[name] = e => this.publish(name, e)
        event.target.addEventListener(name, event.listeners[name])
      })
    })
    window.addEventListener('message', e => {
      this.publish(e.data.name, e.data)
    })
  }

  __hookUtilityPlugins () {
    this.on('mousedown', e => {
      this.clickCheck = true
      let scale = Global.app._options.view.getBoundingClientRect().width / Global.app._options.view.width || 1
      this.publish('mousedownscreen', {
        ...Util.screenToWorld((e.clientX + window.scrollX) / scale, (e.clientY - Global.renderer.view.getBoundingClientRect().y) / scale),
        button: e.button,
        clientX: (e.clientX + window.scrollX) / scale,
        clientY: (e.clientY - Global.renderer.view.getBoundingClientRect().y) / scale
      })
    })
    this.on('mouseup', e => {
      let scale = Global.app._options.view.getBoundingClientRect().width / Global.app._options.view.width || 1
      this.publish('mouseupscreen', {
        ...Util.screenToWorld((e.clientX + window.scrollX) / scale, (e.clientY - Global.renderer.view.getBoundingClientRect().y) / scale),
        button: e.button,
        clientX: (e.clientX + window.scrollX) / scale,
        clientY: (e.clientY - Global.renderer.view.getBoundingClientRect().y) / scale,
        isClick: this.clickCheck
      })
    })
    this.on('mousemove', e => {
      this.clickCheck = false
      let scale = Global.app._options.view.getBoundingClientRect().width / Global.app._options.view.width || 1
      this.publish('mousemovescreen', {
        ...Util.screenToWorld((e.clientX + window.scrollX) / scale, (e.clientY - Global.renderer.view.getBoundingClientRect().y) / scale),
        button: e.button,
        clientX: (e.clientX + window.scrollX) / scale,
        clientY: (e.clientY - Global.renderer.view.getBoundingClientRect().y) / scale
      })
    })
  }

  hookListeners () {
    window.addEventListener('keydown', (e) => {
      if ([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault()
      }
    }, false)
    this.basicEvents = [
      {
        target: window,
        events: ['keydown', 'keyup', 'keypress', 'resize']
      },
      {
        target: Global.canvas,
        events: ['mousedown', 'mouseup', 'mousemove']
      }
    ]
    this.__hookBasicEvents()
    this.__hookUtilityPlugins()
  }

  publish (key, value) {
    if (this.events[key]) {
      this.events[key].forEach(event => event.call(value))
    }
  }

  on (event, callback) {
    if (!this.events[event]) {
      this.events[event] = []
    }
    this.events[event].push(new Event(event, callback))
  }

  off (eventObject) {
    const index = this.events[eventObject.event].indexOf(eventObject)
    if (index >= 0) {
      this.events[eventObject.event].splice(index, 1)
    }
  }
}

export default new EventBus()
