import EventBus from '@/scripts/game/EventBus'

class InputState {
  constructor () {
    this.keys = {}
    EventBus.on('keydown', (e) => {
      this.keys[e.keyCode] = true
    })
    EventBus.on('keyup', (e) => {
      this.keys[e.keyCode] = undefined
    })
  }

  isKeyDown (key) {
    return this.keys[key]
  }
}

export default new InputState()
