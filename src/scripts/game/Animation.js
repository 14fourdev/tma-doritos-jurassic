import ObstacleData from '@/scripts/game/ObstacleData'
import TextureManager from '@/scripts/game/TextureManager'

class Animation {
  constructor (sprite, key) {
    this.counter = 0
    this.sprite = sprite
    this.key = key
    this.animationData = ObstacleData[sprite.customData.key].animations[key]
    this.sprite.customData.animating = true
  }

  update () {
    let frame = Math.floor(this.counter / this.animationData.rate) % this.animationData.frames.length
    this.sprite.texture = TextureManager.sprites[this.animationData.frames[frame]]
    this.iterations = Math.floor(this.counter / (this.animationData.rate * this.animationData.frames.length))
    this.counter += 1
  }

  reset () {
    this.sprite.texture = TextureManager.sprites[this.animationData.frames[0]]
    this.sprite.customData.animating = false
  }
}

export default Animation
