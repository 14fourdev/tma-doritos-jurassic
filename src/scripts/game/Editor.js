import * as PIXI from 'pixi.js'
import EventBus from '@/scripts/game/EventBus'
import InputState from '@/scripts/game/InputState'
import Global from '@/scripts/game/Global'
import PathTextureList from '@/scripts/game/PathTextureList'
import PanZoom from '@/scripts/game/PanZoom'
import MathJS from 'mathjs'
import TextureManager from '@/scripts/game/TextureManager'
import ObstacleData from '@/scripts/game/ObstacleData'
import ItemPickupManager from '@/scripts/game/ItemPickupManager'

class Editor {
  constructor () {
    this.obstacleSprites = []
    this.__init()
    this.__initListeners()
    this.__initDrawControls()
    this.__initStructures()
    this.__setEnabled(false)
  }

  __init () {
    this.selectUI = Global.component.$parent.$refs.editor

    this.editorLayer = new PIXI.Container()
    this.platformLayer = new PIXI.Container()
    this.editorLayer.addChild(this.platformLayer)
    this.gridSize = Global.world.data.gridSize
    this.currentLayer = 0

    this.__initGrid()
    this.__initLevelBounds()
    this.__initEditorUI()
    this.__resetCamera()
  }

  __initEditorUI () {
    this.editorUI = new PIXI.Graphics()
    this.editorUI.position.set(0, 0)
    this.editorLayer.addChild(this.editorUI)
    this.__initUIObstaclePane()
    this.__initUILayerText()
  }

  __initUILayerText () {
    this.layerText = new PIXI.Text('', {
      fontFamily: 'Arial',
      fontSize: 24,
      fill: 0xffffff,
      align: 'left'
    })
    this.__setLayer(0)
    this.layerText.position.set(10, 10)
    this.editorUI.addChild(this.layerText)
  }

  __initUIObstaclePane () {
    this.obstaclePane = new PIXI.Graphics()
    this.editorUI.addChild(this.obstaclePane)
    this.obstaclePane.position.set(0, 0)
    this.obstaclePane
      .beginFill(0x000000, 0.5)
      .drawRect(0, 0, 256 + 16, Global.screen.height)
      .endFill()
    let keys = Object.keys(ObstacleData)
    for (let i = 0; i < keys.length; i++) {
      let x1 = (i % 4) * 64 + 16
      let y1 = Math.floor(i / 4) * 64 + 48
      let data = ObstacleData[keys[i]]
      this.obstaclePane
        .beginFill(data.type === 'decoration' ? 0x003300 : (data.special ? 0x03366 : 0x4a0000), 0.5)
        .drawRect(x1, y1, 48, 48)
        .endFill()
      let sprite = new PIXI.Sprite(TextureManager.sprites[data.texture])
      let ratio = sprite.width / sprite.height
      if (ratio > 1) {
        sprite.width = 48
        sprite.height = 48 / ratio
      } else {
        sprite.height = 48
        sprite.width = 48 * ratio
      }
      sprite.anchor.set(0.5, 0.5)
      sprite.position.set(x1 + 24, y1 + 24)
      sprite.customBounds = {
        x1,
        y1,
        x2: x1 + 48,
        y2: y1 + 48
      }
      sprite.customData = data
      sprite.key = keys[i]
      this.obstacleSprites.push(sprite)
      this.editorUI.addChild(sprite)
    }
  }

  __initGrid () {
    this.debugGrid = new PIXI.Graphics()
    this.debugGrid.position.set(0, 0)
    this.debugGrid.lineStyle(1, 0xffffff)
    this.debugGrid.alpha = 0.05
    for (let x = 0; x < Global.screen.width + this.gridSize * 2; x += this.gridSize) {
      this.debugGrid.moveTo(x, 0)
      this.debugGrid.lineTo(x, Global.screen.height + this.gridSize * 2)
    }
    for (let y = 0; y < Global.screen.height + this.gridSize * 2; y += this.gridSize) {
      this.debugGrid.moveTo(0, y)
      this.debugGrid.lineTo(Global.screen.width + this.gridSize * 2, y)
    }
    this.editorLayer.addChild(this.debugGrid)
  }

  __initLevelBounds () {
    this.levelBoundsDistinguisher = new PIXI.Graphics()
    this.levelBoundsDistinguisher
      .beginFill(0xffffff, 0.05)
      .drawRect(0, 0, Global.world.data.width, Global.world.data.height)
      .endFill()
    this.editorLayer.addChild(this.levelBoundsDistinguisher)
  }

  __resetCamera () {
    Global.camera.pivot.x = Global.screen.width / 2 - this.gridSize * 4
    Global.camera.pivot.y = Global.world.data.height / 2
  }

  __initListeners () {
    PanZoom(Global.canvas, (e) => {
      if (!(InputState.isKeyDown(91) || InputState.isKeyDown(93))) {
        Global.camera.pivot.x -= e.dx
        Global.camera.pivot.y -= e.dy
      }
    })
    EventBus.on('keydown', (e) => {
      // console.log(e.keyCode)
      if (e.keyCode === 192) {
        this.__setEnabled(!this.enabled)
      }
      if (e.keyCode === 49) {
        this.__setLayer(0)
      }
      if (e.keyCode === 50) {
        this.__setLayer(1)
      }
      if (e.keyCode === 51) {
        this.__setLayer(2)
      }
    })
  }

  __setLayer (layer) {
    this.currentLayer = layer
    switch (this.currentLayer) {
      case 0:
        this.layerText.text = `Layer: Top`
        break
      case 1:
        this.layerText.text = `Layer: Middle`
        break
      case 2:
        this.layerText.text = `Layer: Bottom`
        break
    }
  }

  __initDrawControls () {
    EventBus.on('mousedownscreen', (e) => {
      if (this.enabled) {
        if (e.button === 0) {
          if (InputState.isKeyDown(91) || InputState.isKeyDown(93)) {
            this.selectedLinePoint = this.__getSelectedStructureLine(e.x, e.y)
            if (!this.selectedLinePoint) {
              this.selectedLinePoint = this.__createPlatformStructure(Math.round(e.x / this.gridSize) * this.gridSize, Math.round(e.y / this.gridSize) * this.gridSize)
            } else if (this.selectedLinePoint.interpolated) {
              this.selectedLinePoint.structure.points.push(this.selectedLinePoint.point)
              this.__updateStructure(this.selectedLinePoint.structure)
            }
          }
        }
      }
    })
    EventBus.on('mousemovescreen', (e) => {
      if (this.enabled) {
        this.mouseX = e.x
        this.mouseY = e.y
        this.snapMouseX = Math.round(this.mouseX / this.gridSize) * this.gridSize
        this.snapMouseY = Math.round(this.mouseY / this.gridSize) * this.gridSize
        if (this.droppingObstacle) {
          this.lastPoint = this.__getSelectedStructureLine(e.x, e.y)
        }
        if (e.button === 0) {
          if (this.selectedLinePoint) {
            this.selectedLinePoint.point[0] = this.snapMouseX
            this.selectedLinePoint.point[1] = this.snapMouseY
            this.__updateStructure(this.selectedLinePoint.structure)
          }
        }
      }
    })
    EventBus.on('mouseupscreen', (e) => {
      if (this.enabled) {
        if (e.button === 0) {
          this.selectedLinePoint = null
        }
        if (e.isClick) {
          if (e.button === 0) {
            let selectedObstacleSprite = false
            this.obstacleSprites.forEach(sprite => {
              if (sprite.customBounds.x1 < e.clientX && sprite.customBounds.x2 > e.clientX && sprite.customBounds.y1 < e.clientY && sprite.customBounds.y2 > e.clientY) {
                this.__beginDroppingObstacle(sprite)
                selectedObstacleSprite = true
              }
            })
            if (!selectedObstacleSprite) {
              if (this.droppingObstacle) {
                if (this.lastPoint || this.droppingObstacle.__runtime.type === 'decoration') {
                  this.selectUI.select(this.droppingObstacle)
                  this.droppingObstacle.__runtime.graphic.customBounds = this.droppingObstacle.__runtime.graphic.getBounds()
                  this.droppingObstacle.__runtime.graphic.customBounds.x += Global.camera.pivot.x - Global.screen.width / 2
                  this.droppingObstacle = null
                }
              } else {
                if (Global.world.data.obstacles.length > 0) {
                  let closest = [ ...Global.world.data.obstacles ].sort((a, b) => {
                    return MathJS.distance([a.__runtime.graphic.x - (a.__runtime.graphic.anchor.x - 0.5) * a.__runtime.graphic.width, a.__runtime.graphic.y - (a.__runtime.graphic.anchor.y - 0.5) * a.__runtime.graphic.height], [e.x, e.y]) - MathJS.distance([b.x, b.y], [e.x, e.y])
                  })[0]
                  if (MathJS.distance([closest.__runtime.graphic.x - (closest.__runtime.graphic.anchor.x - 0.5) * closest.__runtime.graphic.width, closest.__runtime.graphic.y - (closest.__runtime.graphic.anchor.y - 0.5) * closest.__runtime.graphic.height], [e.x, e.y]) < 150) {
                    this.selectUI.select(closest)
                  } else {
                    this.selectUI.deselect()
                  }
                }
              }
            }
          } else if (e.button === 2) {
            if (this.droppingObstacle) {
              Global.world.removeObstacle(this.droppingObstacle)
              this.droppingObstacle = null
            }
            this.__removePointAtPosition(e.x, e.y)
          }
          console.log('click')
        }
      }
    })
  }

  __initStructures () {
    Global.world.data.structures.forEach(structure => this.__initStructure(structure))
  }

  __initStructure (structure) {
    this.__initPlatform(structure)
  }

  __initPlatform (platform) {
    platform.__runtime.editor = {
      selected: false,
      selectedNode: 0,
      graphic: new PIXI.Graphics()
    }
    this.__updateStructure(platform)
    this.platformLayer.addChild(platform.__runtime.editor.graphic)
  }

  __removePointAtPosition (x, y) {
    const line = this.__getSelectedStructureLine(x, y, true)
    if (line) {
      if (line.structure.points.length > 2) {
        line.structure.points.splice(line.structure.points.indexOf(line.point), 1)
        this.__updateStructure(line.structure)
      } else {
        this.__removeStructure(line.structure)
      }
    }
  }

  __removeStructure (structure) {
    this.platformLayer.removeChild(structure.__runtime.editor.graphic)
    Global.world.removeStructure(structure)
  }

  __beginDroppingObstacle (sprite) {
    if (this.droppingObstacle) {
      Global.world.removeObstacle(this.droppingObstacle)
    }
    const obstacle = {
      key: sprite.key,
      layer: this.currentLayer,
      x: 0,
      y: 0,
      rotation: 0,
      scale: sprite.customData.scale
    }
    Global.world.buildObstacle(obstacle)
    Global.world.data.obstacles.push(obstacle)
    this.droppingObstacle = obstacle
  }

  __createPlatformStructure (x, y) {
    const platform = {
      layer: this.currentLayer,
      points: [
        [x, y],
        [x, y]
      ],
      textures: PathTextureList[this.currentLayer]
    }
    Global.world.data.structures.push(platform)
    Global.world.buildStructure(platform)
    this.__initStructure(platform)
    return { structure: platform, point: platform.points[1] }
  }

  __getSelectedStructureLine (x, y, mainOnly = false) {
    // Check for main points
    for (let i = 0; i < Global.world.data.structures.length; i++) {
      const structure = Global.world.data.structures[i]
      for (let j = 0; j < structure.points.length; j++) {
        const point = structure.points[j]
        if (MathJS.distance(point, [x, y]) < 8) {
          return { structure, point }
        }
      }
    }

    if (!mainOnly) {
      // Check for interpolated points
      for (let i = 0; i < Global.world.data.structures.length; i++) {
        const structure = Global.world.data.structures[i]
        for (let j = 0; j < structure.__runtime.curve.interpolatedPoints.length; j++) {
          const point = structure.__runtime.curve.interpolatedPoints[j]
          if (MathJS.distance(point, [x, y]) < 6) {
            let angle = 0
            if (j < structure.__runtime.curve.interpolatedPoints.length - 1 && j > 0) {
              let left = structure.__runtime.curve.interpolatedPoints[j - 1]
              let right = structure.__runtime.curve.interpolatedPoints[j + 1]
              angle = Math.atan2(right[1] - left[1], right[0] - left[0])
            }
            let pointOut = [point[0], point[1]]
            return { structure, point: pointOut, interpolated: true, angle }
          }
        }
      }
    }

    return null
  }

  __setEnabled (enabled) {
    this.enabled = enabled
    this.selectUI.enabled = enabled
    if (enabled) {
      ItemPickupManager.reset()
      Global.game.counter = -Infinity
      Global.game.paused = true
      Global.component.ended = false
      Global.component.$refs.ui.style.display = 'none'
      Global.camera.addChild(this.editorLayer)
    } else {
      Global.component.$refs.ui.style.display = 'block'
      Global.camera.removeChild(this.editorLayer)
    }
  }

  __update (delta) {
    this.debugGrid.x = Global.camera.pivot.x - (Global.camera.pivot.x % this.gridSize) - Global.screen.width / 2 + ((Global.screen.width / 2) % this.gridSize) - this.gridSize
    this.debugGrid.y = Global.camera.pivot.y - (Global.camera.pivot.y % this.gridSize) - Global.screen.height / 2 + ((Global.screen.height / 2) % this.gridSize) - this.gridSize
    this.editorUI.x = Global.camera.pivot.x - Global.screen.width / 2
    this.editorUI.y = Global.camera.pivot.y - Global.screen.height / 2
    if (this.droppingObstacle) {
      if (this.droppingObstacle.__runtime.type === 'decoration') {
        this.droppingObstacle.x = this.droppingObstacle.__runtime.graphic.x = this.mouseX
        this.droppingObstacle.y = this.droppingObstacle.__runtime.graphic.y = this.mouseY
      } else {
        if (this.lastPoint) {
          this.droppingObstacle.layer = this.droppingObstacle.__runtime.graphic.customData.layer = this.lastPoint.structure.layer
          this.droppingObstacle.x = this.droppingObstacle.__runtime.graphic.x = this.lastPoint.point[0]
          this.droppingObstacle.y = this.droppingObstacle.__runtime.graphic.y = this.lastPoint.point[1] + 16 * this.lastPoint.structure.layer - (this.lastPoint.structure.layer === 0 ? 8 : 0)
          this.droppingObstacle.scale = ObstacleData[this.droppingObstacle.key].scale + ObstacleData[this.droppingObstacle.key].scale * (this.lastPoint.structure.layer / 3)
          this.droppingObstacle.__runtime.graphic.scale.x = this.droppingObstacle.scale
          this.droppingObstacle.__runtime.graphic.scale.y = this.droppingObstacle.scale
          this.droppingObstacle.__runtime.graphic.alpha = 1
          this.droppingObstacle.rotation = this.droppingObstacle.__runtime.graphic.rotation = this.lastPoint.angle || 0
          // Global.game.__sortEntities()
        } else {
          this.droppingObstacle.x = this.droppingObstacle.__runtime.graphic.x = this.mouseX
          this.droppingObstacle.y = this.droppingObstacle.__runtime.graphic.y = this.mouseY
          this.droppingObstacle.__runtime.graphic.alpha = 0.5
          this.droppingObstacle.rotation = this.droppingObstacle.__runtime.graphic.rotation = 0
        }
      }
    }
  }

  __updateStructure (structure) {
    Global.world.updatePlatform(structure)
    this.__updatePlatform(structure)
  }

  __updatePlatform (platform) {
    platform.__runtime.editor.graphic.clear()
    platform.__runtime.editor.graphic.alpha = 0.8
    platform.__runtime.editor.graphic.moveTo(platform.points[0][0], platform.points[0][1])

    for (let i = 0; i < platform.points.length - 1; i++) {
      const xMid = (platform.points[i][0] + platform.points[i + 1][0]) / 2
      const yMid = (platform.points[i][1] + platform.points[i + 1][1]) / 2
      const cpx1 = (xMid + platform.points[i][0]) / 2
      const cpx2 = (xMid + platform.points[i + 1][0]) / 2
      platform.__runtime.editor.graphic.lineStyle(2, 0xffffff)
      platform.__runtime.editor.graphic.quadraticCurveTo(cpx1, platform.points[i][1], xMid, yMid)
      platform.__runtime.editor.graphic.quadraticCurveTo(cpx2, platform.points[i + 1][1], platform.points[i + 1][0], platform.points[i + 1][1])
    }

    platform.points.forEach((point) => {
      platform.__runtime.editor.graphic.lineStyle(1, 0xffffff)
      platform.__runtime.editor.graphic.drawCircle(point[0], point[1], 6)
    })

    platform.__runtime.curve.interpolatedPoints.forEach((point) => {
      platform.__runtime.editor.graphic.lineStyle(1, 0xffffff)
      platform.__runtime.editor.graphic.drawCircle(point[0], point[1], 3)
    })
  }

  saveLevel () {
    const structures = []
    const obstacles = []
    console.log(Global.world.data.obstacles.length)
    Global.world.data.structures.forEach((structure) => {
      structures.push({...structure, __runtime: undefined})
    })
    Global.world.data.obstacles.filter(obstacle => obstacle.key !== 'impactcrater').forEach((obstacle) => {
      obstacles.push({...obstacle, __runtime: undefined})
    })
    console.log(Global.world.data.obstacles.length)
    return {...Global.world.data, structures, obstacles}
  }
}

export { Editor }
