import * as PIXI from 'pixi.js'
import Global from '@/scripts/game/Global'

class DecorationManager {
  constructor () {
    this.decorations = []
    this.container = new PIXI.Container()
  }

  cull () {
    if (process.env.NODE_ENV === 'development') {
      if (Global.editor && Global.editor.enabled) {
        this.container.children.forEach(child => {
          if (child.customBounds) {
            child.visible = true
          }
        })
        return
      }
    }
    this.container.children.forEach(child => {
      if (child.customBounds) {
        child.visible = !(Global.camera.pivot.x + Global.screen.width / 2 < child.customBounds.x || Global.camera.pivot.x - Global.screen.width / 2 > child.customBounds.x + child.customBounds.width)
      }
    })
  }
}

export default new DecorationManager()
