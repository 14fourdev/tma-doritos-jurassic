export default [
  [ // top layer
    'top-path.png'
  ],
  [ // middle layer
    'sand-path.png'
  ],
  [ // bottom layer
    'grass-path.png'
  ]
]
