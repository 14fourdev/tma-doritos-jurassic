import { Howl } from 'howler'

const _SOUNDS = {
  general: [
    { soundKey: 'gyrosphere-jump-landing', volume: 0.4 },
    { soundKey: 'gyrosphere-movement', volume: 0.6, loop: true },
    { soundKey: 'gyrosphere-gravel', volume: 0, loop: true },
    { soundKey: 'gyrosphere-grass', volume: 0, loop: true },
    { soundKey: 'obstacle-hit-bones', volume: 0.8 },
    { soundKey: 'obstacle-hit-gyrosphere', volume: 0.5 },
    { soundKey: 'obstacle-hit-lava', volume: 1 },
    { soundKey: 'obstacle-hit-crater', volume: 1 },
    { soundKey: 'obstacle-hit-rock', volume: 0.4 },
    { soundKey: 'powerup-boost', volume: 0.4 },
    { soundKey: 'powerup-repair', volume: 1 },
    { soundKey: 'stampede', volume: 1, loop: true },
    { soundKey: 'music-background', volume: 1, loop: true },
    { soundKey: 'jw-stinger-intro', volume: 1 },
    { soundKey: 'jw-stinger-lose', volume: 0.9 },
    { soundKey: 'jw-stinger-win', volume: 0.7 },
    { soundKey: 'projectile-debris', volume: 1 }
  ]
}

class AudioManager {
  constructor () {
    this.sounds = {}
    this.preloaded = []
    // document.body.addEventListener('touchend', () => {
    //   Howler.ctx.resume()
    // })
    // setInterval(() => {
    //   console.log(Howler.ctx.state)
    // }, 100)
  }

  load (sKey) {
    if (this.preloaded.indexOf(sKey) < 0) {
      this.preloaded.push(sKey)
      _SOUNDS[sKey].forEach((soundObj) => {
        let soundKey = soundObj.soundKey
        /*eslint-disable */
        // console.log(`Loading sound: ${soundKey}`)
        this.sounds[soundKey] = {
          audio: new Howl({
            src: [`static/sound/ogg/${soundKey}.ogg`, `static/sound/mp3/${soundKey}.mp3`],
            autoplay: false,
            preload: true,
            loop: soundObj.loop,
            volume: soundObj.volume,
            onload: () => {
              // console.log(`Finished loading sound: ${soundKey}`)
            }
          }),
          isActive: false
        }
        // this.sounds[soundKey].audio.bindOnce('canplaythrough', () => {
        //   console.log(`Finished loading sound: ${soundKey}`)
        // })
        // this.sounds[soundKey].audio.bind('ended pause', () => {
        //   this.sounds[soundKey].isActive = false
        // })
        /*eslint-enable */
      })
    }
  }

  play (soundKey) {
    if (!window.isIE11) {
      this.sounds[soundKey].audio.play()
    }
  }

  stop (soundKey) {
    this.sounds[soundKey].audio.stop()
  }

  stopAll () {
    for (let soundkey in this.sounds) {
      this.sounds[soundkey].audio.stop()
    }
  }

  mute (v) {
    for (let soundkey in this.sounds) {
      this.sounds[soundkey].audio.mute(v)
    }
  }
}

export default new AudioManager()
