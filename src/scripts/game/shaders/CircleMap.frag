// x = ½ √( 2 + u² - v² + 2u√2 ) - ½ √( 2 + u² - v² - 2u√2 )
// y = ½ √( 2 - u² + v² + 2v√2 ) - ½ √( 2 - u² + v² - 2v√2 )
// p.5 elliptical grid mapping https://arxiv.org/pdf/1509.06344.pdf
precision mediump float;
uniform vec4 filterArea;
uniform sampler2D uSampler;
uniform mat3 mappedMatrix;
varying vec2 vTextureCoord;
uniform vec2 dimensions;

vec2 mapCoord( vec2 coord )
{
    coord *= filterArea.xy;
    return coord;
}

vec2 unmapCoord( vec2 coord )
{
    coord /= filterArea.xy;
    return coord;
}

void main(void) {
    vec2 coord = vTextureCoord;
    coord = mapCoord(coord) / dimensions;
    float d = distance(coord, vec2(0.5, 0.5));
    if (d < 0.465) {
        float u = coord.x * 2.0 - 1.0;
        float v = coord.y * 2.0 - 1.0;
        float u2 = u * u;
        float v2 = v * v;
        float sq = sqrt(2.0);
        vec2 n = vec2(
            0.5 * sqrt(2.0 + u2 - v2 + 2.0 * u * sq) - 0.5 * sqrt(2.0 + u2 - v2 - 2.0 * u * sq),
            0.5 * sqrt(2.0 - u2 + v2 + 2.0 * v * sq) - 0.5 * sqrt(2.0 - u2 + v2 - 2.0 * v * sq)
        );
        n = n * 0.5 + 0.5;
        n = unmapCoord(n * dimensions);
        vec4 color = texture2D(uSampler, vec2(n.x, n.y));
        gl_FragColor = vec4(color.r, color.g, color.b, 1.0);
    } else {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
    }
}
