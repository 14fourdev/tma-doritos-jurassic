uniform float uTime;
uniform sampler2D uSampler;
varying vec2 vTextureCoord;

void main (void) {
  vec4 color = texture2D(uSampler, vTextureCoord);

  gl_FragColor = vec4(
    color.r * (1.0 + 2.0 * color.r * abs(sin(uTime * 1.0)) + 1.0 * color.r * abs(sin(uTime * 1.5)) + 1.0 * color.r * abs(sin(uTime * 1.7))),
    color.g * (1.0 + 1.0 * color.g * abs(sin(uTime * 1.5)) + 1.0 * color.g * abs(sin(uTime * 1.7))),
    color.b * (1.0 + 1.0 * color.b * abs(sin(uTime * 1.7))),
    color.a
  );
}