import * as PIXI from 'pixi.js'
import Easings from '@/scripts/game/Easings'
import Particle from '@/scripts/game/particles/Particle'

class FallingDebrisExplosionFire extends Particle {
  constructor (x, y, lane) {
    super()
    this.x = x - 60
    this.y = y - 60 - (2 - lane) * 20
    this.angle = Math.random() * Math.PI * 2
    this.speed = Math.random() * 4 - 2
    this.rotRate = Math.random() * 0.1 - 0.05
    this.lane = lane
    this.setTexture('fire.png')
  }

  setTexture (texture) {
    super.setTexture(texture)
    this.sprite.customData.layer = this.lane
    this.sprite.anchor.set(0.5, 0.5)
    this.sprite.blendMode = PIXI.BLEND_MODES.ADD
    this.sprite.rotation = Math.random() * Math.PI * 2
    this.updateSprite()
  }

  update () {
    super.update()
    this.updateSprite()
    this.speed /= 1.02
    if (this.counter > 60) {
      this.destroy()
    }
  }

  updateSprite () {
    let t = Easings.easeOutQuart(this.counter / 60)
    this.sprite.scale.set(0.2 + 1.5 * t)
    this.sprite.alpha = 1 - t
    this.sprite.rotation += this.rotRate
  }
}

export default FallingDebrisExplosionFire
