import * as PIXI from 'pixi.js'
import TextureManager from '@/scripts/game/TextureManager'
import ParticleManager from '@/scripts/game/ParticleManager'

class Particle {
  set angle (v) { this._angle = this.sprite.rotation = v }
  set x (v) { this._x = this.sprite.x = v }
  set y (v) { this._y = this.sprite.y = v }
  get angle () { return this._angle }
  get x () { return this._x }
  get y () { return this._y }

  constructor () {
    this.sprite = {}
    this.speed = 5
    this.angle = 0
    this.counter = 0
    this.xvscale = 1
    this.yvscale = 1
  }

  setTexture (texture) {
    this.sprite = new PIXI.Sprite(TextureManager.sprites[texture])
    this.sprite.x = this.x
    this.sprite.y = this.y
    this.sprite.rotation = this.angle
    this.sprite.customData = {
      layer: 100
    }
  }

  update () {
    this.x += Math.cos(this.angle) * this.speed * this.xvscale
    this.y += Math.sin(this.angle) * this.speed * this.yvscale
    this.counter += 1
  }

  destroy () {
    ParticleManager.removeParticle(this)
  }
}

export default Particle
