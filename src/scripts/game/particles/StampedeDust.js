import * as PIXI from 'pixi.js'
import Easings from '@/scripts/game/Easings'
import Particle from '@/scripts/game/particles/Particle'

class StampedeDust extends Particle {
  constructor (x, y, lane) {
    super()
    this.x = x
    this.y = y
    this.lane = lane
    this.angle = Math.random() * Math.PI * 2
    this.speed = Math.random() * 12 - 6 + 2
    this.rotRate = Math.random() * 0.02 - 0.01
    this.setTexture('stampede-dust.png')
  }

  setTexture (texture) {
    super.setTexture(texture)
    this.yvscale = 0.2
    this.sprite.blendMode = PIXI.BLEND_MODES.NORMAL
    this.sprite.customData.layer = this.lane + 0.15
    this.sprite.anchor.set(0.5, 0.5)
    this.sprite.rotation = Math.random() * Math.PI * 2
    this.updateSprite()
  }

  update () {
    super.update()
    this.updateSprite()
    this.speed /= 1.03
    if (this.counter > 90) {
      this.destroy()
    }
  }

  updateSprite () {
    let t = Easings.easeOutQuad(this.counter / 90)
    this.sprite.scale.set(2 + 2 * t)
    this.sprite.alpha = Easings.easeOutQuad(1 - this.counter / 90, 4)
    this.sprite.rotation += this.rotRate
  }
}

export default StampedeDust
