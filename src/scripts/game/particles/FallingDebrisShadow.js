import Particle from '@/scripts/game/particles/Particle'

class FallingDebrisShadow extends Particle {
  constructor (x, y, lane) {
    super()
    this.x = x - 60
    this.y = y
    this.speed = 0
    this.setTexture('debrisshadow.png')
    this.lane = lane
  }

  setTexture (texture) {
    super.setTexture(texture)
    this.sprite.customData.layer = this.lane
    this.sprite.anchor.set(0.5, 0.5)
  }

  update () {
    super.update()
    this.sprite.scale.set((1 + 1.5 * Math.max(0, (90 - this.counter) / 90)) * ((this.lane / 2 + 1) / 2))
    this.sprite.alpha = Math.max(0, this.counter / 90)
  }
}

export default FallingDebrisShadow
