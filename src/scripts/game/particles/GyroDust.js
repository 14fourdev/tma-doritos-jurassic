// import * as PIXI from 'pixi.js'
import Easings from '@/scripts/game/Easings'
import Particle from '@/scripts/game/particles/Particle'

class GyroDust extends Particle {
  constructor (x, y, lane, alpha) {
    super()
    this.x = x - 20
    this.y = y + 100 - (2 - lane) * 20
    this.alpha = alpha
    // this.angle = Math.random() * Math.PI * 2
    this.angle = 0
    this.speed = Math.random() * 12 - 6 + 2
    this.rotRate = 0
    this.lane = lane
    this.setTexture('gyrodust.png')
  }

  setTexture (texture) {
    super.setTexture(texture)
    this.sprite.customData.layer = this.lane
    this.sprite.anchor.set(0.85, 0.75)
    this.updateSprite()
  }

  update () {
    super.update()
    this.updateSprite()
    this.speed /= 1.03
    // this.y -= (this.counter / 8) * Math.abs(this.speed)
    if (this.counter > 60) {
      this.destroy()
    }
  }

  updateSprite () {
    let t = Easings.easeOutQuad(this.counter / 60)
    let s = 0.2 + 1 * t
    this.sprite.scale.set((s + s * (this.lane / 3)) * this.alpha)
    this.sprite.alpha = (0.5 - t / 2) * this.alpha
    // this.sprite.rotation += this.rotRate
  }
}

export default GyroDust
