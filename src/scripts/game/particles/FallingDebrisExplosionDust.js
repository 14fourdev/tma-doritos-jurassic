// import * as PIXI from 'pixi.js'
import Easings from '@/scripts/game/Easings'
import Particle from '@/scripts/game/particles/Particle'

class FallingDebrisExplosionDust extends Particle {
  constructor (x, y, lane) {
    super()
    this.x = x - 60
    this.y = y - 60 - (2 - lane) * 20
    this.angle = Math.random() * Math.PI * 2
    this.speed = Math.random() * 12 - 6 + 2
    this.rotRate = Math.random() * 0.1 - 0.05
    this.lane = lane
    this.setTexture('dust.png')
  }

  setTexture (texture) {
    super.setTexture(texture)
    this.sprite.customData.layer = this.lane
    this.sprite.anchor.set(0.5, 0.5)
    this.sprite.rotation = Math.random() * Math.PI * 2
    this.updateSprite()
  }

  update () {
    super.update()
    this.updateSprite()
    this.speed /= 1.03
    // this.y -= (this.counter / 8) * Math.abs(this.speed)
    if (this.counter > 120) {
      this.destroy()
    }
  }

  updateSprite () {
    let t = Easings.easeOutQuad(this.counter / 120)
    this.sprite.scale.set(1.2 + 2 * t)
    this.sprite.alpha = 1 - t
    this.sprite.rotation += this.rotRate
  }
}

export default FallingDebrisExplosionDust
