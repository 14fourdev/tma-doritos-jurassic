import ParticleManager from '@/scripts/game/ParticleManager'
import Global from '@/scripts/game/Global'
import ObstacleData from '@/scripts/game/ObstacleData'
import EntityManager from '@/scripts/game/EntityManager'
import AudioManager from '@/scripts/game/AudioManager'
import Particle from '@/scripts/game/particles/Particle'
import FallingDebrisShadow from '@/scripts/game/particles/FallingDebrisShadow'
import FallingDebrisExplosionSmoke from '@/scripts/game/particles/FallingDebrisExplosionSmoke'
import FallingDebrisExplosionDust from '@/scripts/game/particles/FallingDebrisExplosionDust'
import FallingDebrisExplosionFire from '@/scripts/game/particles/FallingDebrisExplosionFire'
// import FallingDebrisDirt from '@/scripts/game/FallingDebrisDirt'

class FallingDebris extends Particle {
  constructor (lane, targetX) {
    super()
    this.lane = lane
    this.targetX = targetX
    this.points = Global.world.getStartEndPointsOfPlatformLineSegment(targetX, lane)
    this.targetY = Global.world.getFloorYOfSegment(targetX, this.points)
    this.angle = Math.PI / 8
    this.x = this.targetX - Math.cos(this.angle) * 2000
    this.y = this.targetY - Math.sin(this.angle) * 2000
    this.speed = 25
    this.setTexture('projectile.png')
    this.__initShadow()
  }

  __initShadow () {
    this.shadow = new FallingDebrisShadow(this.targetX, this.targetY, this.lane)
    ParticleManager.addParticle(this.shadow)
  }

  setTexture (texture) {
    super.setTexture(texture)
    this.sprite.customData.layer = this.lane
    this.sprite.anchor.set(0.95, 0.61)
  }

  destroy () {
    super.destroy()
    this.shadow.destroy()
  }

  update () {
    super.update()
    this.shadow.x = this.x - 60
    this.shadow.y = Global.world.getFloorYOfSegment(this.x - 60, Global.world.getStartEndPointsOfPlatformLineSegment(this.x - 60, this.lane))
    if (this.y > this.targetY) {
      this.impact()
    }
    ParticleManager.addParticle(new FallingDebrisExplosionDust(this.x - 20, this.y + 20, this.lane))
    ParticleManager.addParticle(new FallingDebrisExplosionSmoke(this.x - 20, this.y + 20, this.lane))
  }

  impact () {
    this.x = this.targetX
    this.y = this.targetY
    this.speed = 0
    const obstacle = {
      key: 'impactcrater',
      layer: this.lane,
      x: this.targetX,
      y: this.targetY + 16 * this.lane - (this.lane === 0 ? 8 : 0),
      rotation: 0,
      scale: ObstacleData['impactcrater'].scale + ObstacleData['impactcrater'].scale * (this.lane / 3)
    }
    Global.world.buildObstacle(obstacle)
    Global.world.data.obstacles.push(obstacle)
    EntityManager.player.__setPotentialObstacles(EntityManager.player.lane, true)
    AudioManager.play('projectile-debris')
    this.makeExplosion()
    this.destroy()
  }

  makeExplosion () {
    Global.game.screenShake(this.targetX, 10)
    for (let i = 0; i < 10; i++) {
      let particle = new FallingDebrisExplosionDust(this.targetX, this.targetY, this.lane)
      ParticleManager.addParticle(particle)
    }
    for (let i = 0; i < 20; i++) {
      let particle = new FallingDebrisExplosionSmoke(this.targetX, this.targetY, this.lane)
      ParticleManager.addParticle(particle)
    }
    for (let i = 0; i < 2; i++) {
      let particle = new FallingDebrisExplosionFire(this.targetX, this.targetY, this.lane + 0.1)
      ParticleManager.addParticle(particle)
    }
  }
}

export default FallingDebris
