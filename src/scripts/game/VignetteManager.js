import * as PIXI from 'pixi.js'
import Global from '@/scripts/game/Global'
import TextureManager from '@/scripts/game/TextureManager'
import ParticleManager from '@/scripts/game/ParticleManager'
import EntityManager from '@/scripts/game/EntityManager'
import StampedeDust from '@/scripts/game/particles/StampedeDust'

class VignetteManager {
  constructor () {
    this.container = new PIXI.Container()
    this.counter = 0
  }

  init () {
    this.vignettes = {
      constant: new PIXI.Sprite(TextureManager.sprites['vignette-constant.png']),
      doom: new PIXI.Sprite(TextureManager.sprites['vignette-doom.png'])
    }
    this.vignettes.doom.blendMode = PIXI.BLEND_MODES.ADD
    this.container.addChild(this.vignettes.constant)
    this.container.addChild(this.vignettes.doom)
  }

  update () {
    // ParticleManager.addParticle(new StampedeDust(EntityManager.player.sprite.x, EntityManager.player.sprite.y, EntityManager.player.lane))
    this.vignettes.constant.x = Global.camera.pivot.x - Global.screen.width / 2
    this.vignettes.constant.y = Global.camera.pivot.y - Global.screen.height / 2
    this.vignettes.doom.x = Global.camera.pivot.x - Global.screen.width / 2 - this.vignettes.doom.width + this.vignettes.doom.width * Global.game.shakeMultiplier
    this.vignettes.doom.y = Global.camera.pivot.y - Global.screen.height / 2
    this.vignettes.doom.alpha = Global.game.shakeMultiplier
    if (Global.game.shakeMultiplier > 0) {
      let distanceToEdge = Global.screen.width / 2 + (EntityManager.player.sprite.x - Global.camera.pivot.x)
      for (let i = 0; i < Math.ceil(distanceToEdge / (600 - 300 * Global.game.shakeMultiplier)); i++) {
        let lane = Math.floor(Math.random() * 3)
        let px = EntityManager.player.sprite.x - 100 - distanceToEdge + (distanceToEdge * Global.game.shakeMultiplier + Global.game.shakeMultiplier * 200) * Math.random()
        let py = Global.world.getLaneY(px, lane)
        ParticleManager.addParticle(new StampedeDust(px, py, lane))
      }
    }
    this.counter += 1
  }

  reset () {
    this.counter = 0
  }
}

export default new VignetteManager()
