// import * as PIXI from 'pixi.js'
import FallingDebris from '@/scripts/game/particles/FallingDebris'
import Global from '@/scripts/game/Global'
import EntityManager from '@/scripts/game/EntityManager'

class ParticleManager {
  constructor () {
    this.counter = 0
    this.debrisMinimumDistanceToObstacles = 300
    this.debrisFrequency = 300
    this.debrisLeadDistance = 1750
    this.particles = []
    this.removalQueue = []
  }

  init () {
    this.container = EntityManager.container
    // if (process.env.NODE_ENV === 'development') {
    //   EventBus.on('keydown', (e) => {
    //     if (e.keyCode === 90) {
    //       let targetX = Global.camera.pivot.x + 100
    //       let targetY = Global.camera.pivot.y + 100
    //       for (let i = 0; i < 10; i++) {
    //         let particle = new FallingDebrisExplosionDust(targetX, targetY, 2)
    //         this.addParticle(particle)
    //       }
    //       for (let i = 0; i < 20; i++) {
    //         let particle = new FallingDebrisExplosionSmoke(targetX, targetY, 2)
    //         this.addParticle(particle)
    //       }
    //       for (let i = 0; i < 2; i++) {
    //         let particle = new FallingDebrisExplosionFire(targetX, targetY, 2.5)
    //         this.addParticle(particle)
    //       }
    //     }
    //   })
    // }
  }

  update () {
    this.spawnFallingDebrisCheck()
    this.particles.forEach(particle => {
      particle.update()
    })
    this.removalQueue.forEach(particle => {
      this.spliceParticle(particle)
    })
    this.removalQueue = []
    this.counter += 1
  }

  addParticle (particle) {
    this.particles.push(particle)
    this.container.addChild(particle.sprite)
  }

  removeParticle (particle) {
    this.removalQueue.push(particle)
  }

  spliceParticle (particle) {
    this.particles.splice(this.particles.indexOf(particle), 1)
    this.container.removeChild(particle.sprite)
  }

  spawnFallingDebrisCheck () {
    if ((this.counter + 1) % this.debrisFrequency === 0) { // 300
      let closest = Global.world.getClosestObstacle(EntityManager.player.sprite.x + this.debrisLeadDistance, EntityManager.player.lane)
      if (closest) {
        let distance = Math.abs(closest.x - (EntityManager.player.sprite.x + this.debrisLeadDistance))
        if (distance < this.debrisMinimumDistanceToObstacles) {
          this.counter -= 10
          return
        }
      }
      this.addFallingDebris(EntityManager.player.lane, EntityManager.player.sprite.x + this.debrisLeadDistance)
    }
  }

  addFallingDebris (lane, x) {
    if (x < Global.world.data.width - this.debrisLeadDistance) {
      let debris = new FallingDebris(lane, x)
      this.addParticle(debris)
    }
  }

  reset () {
    this.counter = 0
    this.particles.forEach(particle => {
      particle.destroy()
    })
    this.removalQueue.forEach(particle => {
      this.spliceParticle(particle)
    })
    this.removalQueue = []
  }
}

export default new ParticleManager()
