import Global from '@/scripts/game/Global'

class Util {
  screenToWorld (x, y) {
    return {
      x: Math.round(x + Global.camera.pivot.x - Global.screen.width / 2),
      y: Math.round(y + Global.camera.pivot.y - Global.screen.height / 2)
    }
  }
}

export default new Util()
