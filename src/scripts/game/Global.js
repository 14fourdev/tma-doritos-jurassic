export default {
  component: null, // Vue component
  game: null, // JurassicGame instance
  app: null, // PIXI Application
  camera: null, // PIXI Container
  world: null, // World instance
  editor: null, // Editor instance
  screen: null, // app.screen
  renderer: null // app.renderer
}
