import Animation from '@/scripts/game/Animation'

class AnimationManager {
  constructor () {
    this.animations = []
  }

  animate (sprite, key, iterations = Infinity) {
    let animation = new Animation(sprite, key)
    animation.maxIterations = iterations
    this.animations.push(animation)
  }

  stop (sprite) {
    this.animations = this.animations.filter(animation => animation.sprite !== sprite)
  }

  removeAnimation (animation) {
    animation.sprite.customData.animating = false
    this.animations.splice(this.animations.indexOf(animation), 1)
  }

  update () {
    let removalQueue = []
    this.animations.forEach(animation => {
      animation.update()
      if (animation.iterations >= animation.maxIterations) {
        removalQueue.push(animation)
      }
    })
    removalQueue.forEach(animation => this.removeAnimation(animation))
  }

  reset () {
    this.animations.forEach(animation => animation.reset())
    this.animations = []
  }
}

export default new AnimationManager()
