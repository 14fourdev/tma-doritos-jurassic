import AdaptiveQuadratic from 'adaptive-quadratic-curve'
import MathJS from 'mathjs'

class CurvePath {
  constructor (points) {
    this.points = points
    this.interpolatedPoints = this.getInterpolatedPoints()
    this.distributePoints()
    this.updateLength()
  }

  getHeight (x) {
    // let leftPoint = null
    // let rightPoint = null
    // for (let i = 1; i < this.points.length; i++) {
    //   if (this.points[i][0] > x) {
    //     leftPoint = this.points[i - 1]
    //     rightPoint = this.points[i]
    //     break
    //   }
    // }
    // console.log(leftPoint, rightPoint)
  }

  distributePoints () {
    // Evenly distribute the points by iteratively moving them toward their furthest neighbor
    for (let iterations = 0; iterations < 20; iterations++) {
      for (let i = 1; i < this.interpolatedPoints.length - 1; i++) {
        let leftPoint = this.interpolatedPoints[i - 1]
        let currentPoint = this.interpolatedPoints[i]
        let rightPoint = this.interpolatedPoints[i + 1]

        let leftDistance = MathJS.distance(currentPoint, leftPoint)
        let rightDistance = MathJS.distance(currentPoint, rightPoint)

        let angle = (leftDistance > rightDistance)
          ? Math.atan2(leftPoint[1] - currentPoint[1], leftPoint[0] - currentPoint[0])
          : Math.atan2(rightPoint[1] - currentPoint[1], rightPoint[0] - currentPoint[0])

        currentPoint[0] += Math.cos(angle) * 2
        currentPoint[1] += Math.sin(angle) * 2
      }
    }
  }

  updateLength () {
    const points = this.interpolatedPoints
    let length = 0
    for (let i = 0; i < points.length - 1; i++) {
      length += MathJS.distance(points[i], points[i + 1])
    }
    this.length = length
    if (length >= 2048) {
      console.warn(`Platform exceeds max length by ${length - 2048}`)
      console.warn(this.points)
    }
  }

  getInterpolatedPoints () {
    let pointsOut = []
    for (let i = 0; i < this.points.length - 1; i++) {
      const xMid = (this.points[i][0] + this.points[i + 1][0]) / 2
      const yMid = (this.points[i][1] + this.points[i + 1][1]) / 2
      const cpx1 = (xMid + this.points[i][0]) / 2
      const cpx2 = (xMid + this.points[i + 1][0]) / 2
      pointsOut = pointsOut.concat(AdaptiveQuadratic(
        [this.points[i][0], this.points[i][1]],
        [cpx1, this.points[i][1]],
        [xMid, yMid],
        1
      ))
      pointsOut = pointsOut.concat(AdaptiveQuadratic(
        [xMid, yMid],
        [cpx2, this.points[i + 1][1]],
        [this.points[i + 1][0], this.points[i + 1][1]],
        1
      ))
    }
    return this.removeDuplicates(pointsOut)
  }

  removeDuplicates (points) {
    for (let i = points.length - 1; i >= 1; i--) {
      if (points[i][0] === points[i - 1][0] && points[i][1] === points[i - 1][1]) {
        points.splice(i, 1)
      }
    }
    return points
  }
}

export default CurvePath
