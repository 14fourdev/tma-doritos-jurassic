class Event {
  constructor (event, callback) {
    this.event = event
    this.callback = callback
  }

  call (value) {
    this.callback(value)
  }
}

export default Event
