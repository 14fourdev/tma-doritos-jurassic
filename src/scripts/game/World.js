import axios from 'axios'
import CurvePath from '@/scripts/game/CurvePath'
import TextureManager from '@/scripts/game/TextureManager'
import DecorationManager from '@/scripts/game/DecorationManager'
import EntityManager from '@/scripts/game/EntityManager'
import Global from '@/scripts/game/Global'
import ParallaxBackgroundConfig from '@/scripts/game/ParallaxBackgroundConfig'
import ObstacleData from '@/scripts/game/ObstacleData'
// import Uniforms from '@/scripts/game/shaders/Uniforms'
import * as PIXI from 'pixi.js'

class World {
  constructor () {
    this.counter = 0
    this.container = [
      new PIXI.Container(),
      new PIXI.Container(),
      new PIXI.Container()
    ]
    this.parallax = {}
    this.container.forEach(container => {
      container.pivot.y = -100
    })
  }

  load (path) {
    if (process.env.NODE_ENV === 'development') {
      let storedWorld = window.localStorage.getItem('world')
      if (storedWorld) {
        return new Promise((resolve, reject) => {
          this.buildWorld(JSON.parse(storedWorld)).then(resolve)
        })
      }
    }
    return new Promise((resolve, reject) => {
      axios.get(path).then((response) => {
        this.buildWorld(response.data).then(resolve)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  buildWorld (worldData) {
    return new Promise((resolve) => {
      this.data = worldData
      TextureManager.buildTexturesFromWorld().then(() => {
        ParallaxBackgroundConfig.forEach(parallax => {
          this.parallax[parallax.key] = { ...parallax }
          if (parallax.mode === 'single') {
            this.parallax[parallax.key].sprite = new PIXI.Sprite(TextureManager.sprites[parallax.texture], parallax.dimensions.width / Math.abs(parallax.scale), parallax.dimensions.height)
          } else {
            this.parallax[parallax.key].sprite = new PIXI.extras.TilingSprite(TextureManager.sprites[parallax.texture], parallax.dimensions.width / Math.abs(parallax.scale), parallax.dimensions.height)
          }
          this.parallax[parallax.key].sprite.anchor.set(0.5, 1)
          this.parallax[parallax.key].sprite.scale.set(parallax.scale, Math.abs(parallax.scale))
        })
        this.data.structures.forEach(structure => this.buildStructure(structure))
        this.data.obstacles.forEach(obstacle => this.buildObstacle(obstacle))
        // Global.game.__sortEntities()
        this.structuresReady = true
        resolve()
      })
    })
  }

  buildStructure (structure) {
    structure.__runtime = {}
    this.updatePlatform(structure)
    this.buildPlatform(structure)
  }

  buildObstacle (obstacle) {
    let globalObstacle = ObstacleData[obstacle.key]
    obstacle.__runtime = {
      type: globalObstacle.type,
      texture: globalObstacle.texture,
      special: globalObstacle.special,
      priority: globalObstacle.priority,
      scale: globalObstacle.scale,
      bounds: globalObstacle.bounds,
      anchor: {
        x: globalObstacle.anchor.x,
        y: globalObstacle.anchor.y
      }
    }
    let graphic = obstacle.__runtime.graphic = new PIXI.Sprite(TextureManager.sprites[obstacle.__runtime.texture])
    // if (globalObstacle.shader) {
    //   graphic.filters = [new PIXI.Filter('', globalObstacle.shader, Uniforms.lavapit)]
    // }
    graphic.x = obstacle.x
    graphic.y = obstacle.y
    graphic.rotation = obstacle.rotation
    graphic.anchor.set(obstacle.__runtime.anchor.x, obstacle.__runtime.anchor.y)
    graphic.scale.set(obstacle.scale || obstacle.__runtime.scale, Math.abs(obstacle.scale) || Math.abs(obstacle.__runtime.scale))
    graphic.customBounds = graphic.getBounds()
    graphic.customData = {
      layer: obstacle.layer,
      priority: obstacle.__runtime.priority,
      key: obstacle.key
    }
    if (obstacle.__runtime.type === 'decoration') {
      DecorationManager.container.addChild(graphic)
    } else {
      EntityManager.container.addChild(graphic)
    }
  }

  getClosestObstacle (x, lane) {
    let obs = this.data.obstacles.filter(obstacle => obstacle.layer === lane)
    if (obs.length) {
      obs.sort((a, b) => Math.abs(a.x - x) - Math.abs(b.x - x))
      return obs[0]
    }
    return null
  }

  getLaneY (x, lane) {
    return this.getFloorYOfSegment(x, this.getStartEndPointsOfPlatformLineSegment(x, lane))
  }

  getStartEndPointsOfPlatformLineSegment (x, laneSource, laneTarget, interpolation = 0) {
    let pointsOut = []
    this.data.structures.filter(structure => (structure.layer === laneSource || (laneTarget !== undefined && structure.layer === laneTarget))).forEach(structure => {
      let startX = structure.__runtime.curve.interpolatedPoints[0][0]
      let endX = structure.__runtime.curve.interpolatedPoints[structure.__runtime.curve.interpolatedPoints.length - 1][0]
      if (x >= startX && x < endX) {
        // player is atop this structure
        // TODO: use a binary search for efficiency
        let points = structure.__runtime.curve.interpolatedPoints
        for (let j = 0; j < points.length - 1; j++) { // for each point pair
          if (x >= points[j][0] && x < points[j + 1][0]) { // check if player x is between point pair
            pointsOut.push([points[j], points[j + 1]])
            break
          }
        }
      }
    })
    if (pointsOut.length === 2) {
      return [
        [
          pointsOut[0][0][0] * (1 - interpolation) + pointsOut[1][0][0] * (interpolation),
          pointsOut[0][0][1] * (1 - interpolation) + pointsOut[1][0][1] * (interpolation)
        ],
        [
          pointsOut[0][1][0] * (1 - interpolation) + pointsOut[1][1][0] * (interpolation),
          pointsOut[0][1][1] * (1 - interpolation) + pointsOut[1][1][1] * (interpolation)
        ]
      ]
    } else if (pointsOut.length === 1) {
      return pointsOut[0]
    } else if (pointsOut.length > 2) {
      return [ // there are overlapping structures, but the first 2 will be of the same layer, so take the first and third
        [
          pointsOut[0][0][0] * (1 - interpolation) + pointsOut[2][0][0] * (interpolation),
          pointsOut[0][0][1] * (1 - interpolation) + pointsOut[2][0][1] * (interpolation)
        ],
        [
          pointsOut[0][1][0] * (1 - interpolation) + pointsOut[2][1][0] * (interpolation),
          pointsOut[0][1][1] * (1 - interpolation) + pointsOut[2][1][1] * (interpolation)
        ]
      ]
    } else {
      return null
    }
  }

  getFloorYOfSegment (x, points) {
    let t = (x - points[0][0]) / (points[1][0] - points[0][0])
    return points[0][1] * (1 - t) + points[1][1] * t
  }

  removeStructure (structure) {
    this.container[structure.layer].removeChild(structure.__runtime.tilingSprite)
    this.container[structure.layer].removeChild(structure.__runtime.tilingSpriteLeft)
    this.container[structure.layer].removeChild(structure.__runtime.tilingSpriteRight)
    this.container[structure.layer].removeChild(structure.__runtime.skirt)
    this.container[structure.layer].removeChild(structure.__runtime.graphic)
    this.data.structures.splice(this.data.structures.indexOf(structure), 1)
  }

  removeObstacle (obstacle) {
    Global.game.layers[obstacle.__runtime.type === 'decoration' ? 'decoration' : 'entity'].removeChild(obstacle.__runtime.graphic)
    this.data.obstacles.splice(this.data.obstacles.indexOf(obstacle), 1)
  }

  update () {
    this.counter += 1
    this.cullWorld()
    this.updateBackground()
  }

  updateBackground () {
    let normX = Global.camera.pivot.x
    for (let key in this.parallax) {
      let parallax = this.parallax[key]
      if (parallax.mode === 'single') {
        // let interval = -Math.floor(((parallax.multiplier.x * Global.camera.pivot.x - Global.screen.width / 2 + this.counter * parallax.velocity.x + parallax.offset.x) / -parallax.scale + Global.screen.width / 2) / (parallax.dimensions.width * Math.abs(parallax.scale) + parallax.padding.x))
        // let rand = ((interval * 9301 + 49297) % 233280) / 233280
        parallax.sprite.x = ((parallax.multiplier.x * Global.camera.pivot.x - Global.screen.width / 2 + this.counter * parallax.velocity.x + parallax.offset.x) / -Math.abs(parallax.scale) + Global.screen.width / 2) % (parallax.dimensions.width * Math.abs(parallax.scale) + parallax.padding.x) + Global.camera.pivot.x + parallax.dimensions.width * Math.abs(parallax.scale)
        parallax.sprite.y = parallax.multiplier.y * Global.camera.pivot.y + parallax.offset.y
        // parallax.scale.x =
      } else {
        parallax.sprite.x = normX
        parallax.sprite.y = parallax.multiplier.y * Global.camera.pivot.y + parallax.offset.y
        parallax.sprite.tilePosition.x = (parallax.multiplier.x * Global.camera.pivot.x - Global.screen.width / 2 + this.counter * parallax.velocity.x + parallax.offset.x) / -parallax.scale
        parallax.sprite.tilePosition.x %= parallax.sprite.texture.orig.width
      }
    }
  }

  cullWorld () {
    if (this.structuresReady) {
      this.container.forEach(container => {
        container.children.forEach(child => {
          if (!child.customBounds) {
            console.warn('Custom bounds not defined for', child)
          }
          if (Global.camera.pivot.x + Global.screen.width / 2 < child.customBounds.x || Global.camera.pivot.x - Global.screen.width / 2 > child.customBounds.x + child.customBounds.width) {
          // if (Global.camera.pivot.x < child.customBounds.x || Global.camera.pivot.x > (child.customBounds.x + child.customBounds.width)) {
            child.visible = false
          } else {
            // console.log(Global.camera.pivot.x - Global.screen.width / 2, child.x, child.x + child.width)
            child.visible = true
          }
        })
      })
    }
  }

  reset () {
    this.data.obstacles
      .filter(obstacle => obstacle.key === 'impactcrater')
      .forEach(obstacle => this.removeObstacle(obstacle))
  }

  updatePlatform (structure) {
    structure.points.sort((a, b) => {
      return a[0] - b[0]
    })
    structure.__runtime.curve = new CurvePath(structure.points)
    this.buildPlatform(structure)
  }

  buildPlatform (platform) {
    TextureManager.buildPlatform(platform)
    // if (platform.layer === 0) {
    this.buildSkirt(platform)
    // }
    if (platform.__runtime.graphic) {
      this.container[platform.layer].removeChild(platform.__runtime.graphic)
    }
    platform.__runtime.graphic = new PIXI.mesh.Rope(platform.__runtime.texture, platform.__runtime.curve.interpolatedPoints.reduce((o, i) => {
      o.push(new PIXI.Point(i[0], i[1]))
      return o
    }, []))
    platform.__runtime.graphic.points[0].x -= 16
    platform.__runtime.graphic.points[platform.__runtime.graphic.points.length - 1].x += 16
    platform.__runtime.graphic.customBounds = platform.__runtime.graphic.getBounds()
    this.container[platform.layer].addChild(platform.__runtime.graphic)
  }

  buildSkirt (platform) {
    const firstPoint = platform.__runtime.curve.points[0]
    const lastPoint = platform.__runtime.curve.points[platform.__runtime.curve.points.length - 1]

    if (platform.__runtime.skirt) {
      this.container[platform.layer].removeChild(platform.__runtime.tilingSprite)
      // this.container[platform.layer].removeChild(platform.__runtime.tilingSpriteLeft)
      // this.container[platform.layer].removeChild(platform.__runtime.tilingSpriteRight)
      this.container[platform.layer].removeChild(platform.__runtime.skirt)
    }

    // create tiling sprites
    // center
    platform.__runtime.tilingSprite = new PIXI.extras.TilingSprite(
      TextureManager.sprites[this.data.skirt],
      lastPoint[0] - firstPoint[0],
      this.data.height
    )
    platform.__runtime.tilingSprite.position.x = firstPoint[0]
    platform.__runtime.tilingSprite.position.y = 0
    platform.__runtime.tilingSprite.customBounds = platform.__runtime.tilingSprite.getBounds()

    // left
    // platform.__runtime.tilingSpriteLeft = new PIXI.extras.TilingSprite(
    //   TextureManager.sprites[this.data.skirt],
    //   TextureManager.sprites[this.data.skirt].width,
    //   this.data.height - firstPoint[1]
    // )
    // platform.__runtime.tilingSpriteLeft.position.x = firstPoint[0]
    // platform.__runtime.tilingSpriteLeft.position.y = firstPoint[1]
    // platform.__runtime.tilingSpriteLeft.tilePosition.y = -firstPoint[1]

    // right
    // platform.__runtime.tilingSpriteRight = new PIXI.extras.TilingSprite(
    //   TextureManager.sprites[this.data.skirt],
    //   TextureManager.sprites[this.data.skirt].width,
    //   this.data.height - lastPoint[1]
    // )
    // platform.__runtime.tilingSpriteRight.position.x = lastPoint[0] - TextureManager.sprites[this.data.skirt].width
    // platform.__runtime.tilingSpriteRight.position.y = lastPoint[1]
    // platform.__runtime.tilingSpriteRight.tilePosition.y = -lastPoint[1]

    // create polygon
    let points = platform.__runtime.curve.interpolatedPoints.reduce((o, i) => {
      o.push(new PIXI.Point(i[0], i[1]))
      return o
    }, [])
    points.unshift(new PIXI.Point(platform.__runtime.curve.interpolatedPoints[0][0], this.data.height))
    points.push(new PIXI.Point(platform.__runtime.curve.interpolatedPoints[platform.__runtime.curve.interpolatedPoints.length - 1][0], this.data.height))
    platform.__runtime.skirt = new PIXI.Graphics()
    platform.__runtime.skirt.beginFill(0xffffff)
    platform.__runtime.skirt.drawPolygon(points)
    platform.__runtime.skirt.endFill()
    platform.__runtime.tilingSprite.mask = platform.__runtime.skirt
    platform.__runtime.skirt.customBounds = platform.__runtime.skirt.getBounds()

    this.container[platform.layer].addChild(platform.__runtime.tilingSprite)
    this.container[platform.layer].addChild(platform.__runtime.skirt)
    // this.container[platform.layer].addChild(platform.__runtime.tilingSpriteLeft)
    // this.container[platform.layer].addChild(platform.__runtime.tilingSpriteRight)
  }
}

export default World
