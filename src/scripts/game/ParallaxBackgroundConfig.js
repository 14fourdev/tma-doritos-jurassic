export default [
  {
    key: 'background',
    texture: 'jurassicsky.png',
    foreground: false,
    z: 0,
    dimensions: { width: 1280, height: 1200 },
    multiplier: { x: 0, y: 1 },
    offset: { x: 0, y: 140 },
    velocity: { x: 0, y: 0 },
    scale: 1
  },
  {
    key: 'backgroundClouds',
    texture: 'clouds.png',
    foreground: false,
    z: 1,
    dimensions: { width: 2048, height: 580 },
    multiplier: { x: 0, y: 1 },
    offset: { x: 0, y: 40 },
    velocity: { x: 0.05, y: 0 },
    scale: 1
  },
  {
    key: 'background2',
    texture: 'mountaintile.png',
    foreground: false,
    z: 2,
    dimensions: { width: 2048, height: 490 },
    multiplier: { x: 0.02, y: 1 },
    offset: { x: 1600, y: 140 },
    velocity: { x: 0, y: 0 },
    scale: 1
  },
  {
    key: 'volcano',
    texture: 'volcano.png',
    foreground: false,
    z: 2,
    dimensions: { width: 2048, height: 632 },
    multiplier: { x: 0.02, y: 1 },
    offset: { x: 500, y: 40 },
    velocity: { x: 0, y: 0 },
    scale: 1
  },
  // {
  //   key: 'backgroundMist',
  //   texture: 'clouds2.png',
  //   foreground: false,
  //   z: 3,
  //   dimensions: { width: 2048, height: 580 },
  //   multiplier: { x: 0.02, y: 1 },
  //   offset: { x: 200, y: 340 },
  //   velocity: { x: 0.1, y: 0 },
  //   scale: 1
  // },
  {
    key: 'background3',
    texture: 'verydistantfoliagelikerollinghills.png',
    foreground: false,
    z: 4,
    dimensions: { width: 2048, height: 166 },
    multiplier: { x: 0.065, y: 0.95 },
    offset: { x: 0, y: -60 },
    velocity: { x: 0, y: 0 },
    scale: 0.8
  },
  {
    key: 'background4',
    texture: 'verydistantfoliage.png',
    foreground: false,
    z: 5,
    dimensions: { width: 2048, height: 301 },
    multiplier: { x: 0.15, y: 0.85 },
    offset: { x: 400, y: 60 },
    velocity: { x: 0, y: 0 },
    scale: -1
  },
  {
    key: 'background5',
    texture: 'verydistantfoliageNT.png',
    foreground: false,
    z: 5,
    dimensions: { width: 2048, height: 512 },
    multiplier: { x: 0.4, y: 0.5 },
    offset: { x: -2800, y: 420 },
    velocity: { x: 0, y: 0 },
    scale: 1
  },
  {
    key: 'foreground',
    texture: 'foregroundfoliage.png',
    foreground: true,
    z: 0,
    dimensions: { width: 1280, height: 355 },
    multiplier: { x: 2, y: 0.4 },
    offset: { x: 0, y: 650 },
    velocity: { x: 0, y: 0 },
    scale: 1
  },
  {
    key: 'foreground2',
    texture: 'foregroundfoliage2.png',
    foreground: true,
    z: 1,
    dimensions: { width: 1280, height: 355 },
    multiplier: { x: 3, y: -0.1 },
    offset: { x: 0, y: 1000 },
    velocity: { x: 0, y: 0 },
    scale: -1.25
  },
  {
    key: 'extremetree1',
    texture: 'extremetree1.png',
    mode: 'single',
    padding: {
      x: 3000 * 9
    },
    foreground: true,
    z: 1,
    dimensions: { width: 1796, height: 720 },
    multiplier: { x: 4, y: 0.1 },
    offset: { x: 2450, y: 620 },
    velocity: { x: 0, y: 0 },
    scale: 1.35
  },
  {
    key: 'extremetree2',
    texture: 'extremetree2.png',
    mode: 'single',
    padding: {
      x: 3600 * 9
    },
    foreground: true,
    z: 1,
    dimensions: { width: 972, height: 720 },
    multiplier: { x: 5, y: 0.1 },
    offset: { x: 4660, y: 660 },
    velocity: { x: 0, y: 0 },
    scale: 1.45
  },
  // {
  //   key: 'extremetree3',
  //   texture: 'extremetree1.png',
  //   mode: 'single',
  //   padding: {
  //     x: 2000
  //   },
  //   foreground: true,
  //   z: 1,
  //   dimensions: { width: 1796, height: 720 },
  //   multiplier: { x: 6, y: 0.1 },
  //   offset: { x: 0, y: 660 },
  //   velocity: { x: 0, y: 0 },
  //   scale: -1.75
  // },
  {
    key: 'extremetree4',
    texture: 'extremetree2.png',
    mode: 'single',
    padding: {
      x: 1200 * 9
    },
    foreground: true,
    z: 1,
    dimensions: { width: 972, height: 720 },
    multiplier: { x: 7, y: 0.1 },
    offset: { x: 5660, y: 660 },
    velocity: { x: 0, y: 0 },
    scale: -1.55
  }
]
