import Global from '@/scripts/game/Global'
import Easings from '@/scripts/game/Easings'
import AnimationManager from '@/scripts/game/AnimationManager'

class ItemPickupManager {

  constructor () {
    this.activeAnimations = []
    this.finishedAnimations = []
  }

  collectObstacle (obstacle) {
    this.getYOffset()
    if (!obstacle.__runtime.touched) {
      obstacle.__runtime.touched = true
      switch (obstacle.__runtime.special) {
        case 'repair':
          this.push(obstacle, { animationCounter: 100 })
          Global.game.addHealth(25)
          obstacle.__runtime.graphic.customData.layer += 0.5
          // Global.game.__sortEntities()
          break
        case 'boost':
          this.push(obstacle, { animationCounter: 100 })
          Global.game.addBoost(50)
          obstacle.__runtime.graphic.customData.layer += 0.5
          // Global.game.__sortEntities()
          break
      }
    }
  }

  getYOffset () {
    let rect = Global.app.renderer.view.getBoundingClientRect()
    let scale = rect.width / 1280
    this.yOffset = Math.max(-200, Math.min(0, rect.y / scale))
  }

  updateObstacle (obstacle) {
    let counter = obstacle.__runtime.specialOpts.animationCounter
    let sprite = obstacle.__runtime.graphic
    let t = 1 - counter / obstacle.__runtime.specialOpts.maxCounter
    let tQ = Easings.easeOutQuad(t)
    switch (obstacle.__runtime.special) {
      case 'repair':
      case 'boost':
        if (tQ < 0.5) {
          let tQn = tQ * 2
          let tQi = 1 - tQn
          sprite.x = obstacle.x * tQi + (Global.camera.pivot.x) * tQn
          sprite.y = obstacle.y * tQi + (Global.camera.pivot.y - Global.screen.height / 4 - this.yOffset) * tQn
          sprite.scale.x = sprite.scale.y = Math.abs(obstacle.scale) * tQi + tQn
          sprite.rotation = obstacle.rotation * tQi
        } else {
          if (tQ < 0.6 && !obstacle.__runtime.graphic.customData.animating) {
            AnimationManager.animate(obstacle.__runtime.graphic, 'sheen', 1)
          }
          if (tQ >= 0.875) {
            sprite.alpha = 1 - Easings.easeInQuad((tQ - 0.875) * 8)
          }
          sprite.x = Global.camera.pivot.x
          sprite.y = Global.camera.pivot.y - Global.screen.height / 4 - this.yOffset
        }
        sprite.customBounds = sprite.getBounds()
        sprite.customBounds.x += Global.camera.pivot.x - Global.screen.width / 2
        sprite.customBounds.y += Global.camera.pivot.y - Global.screen.height / 2
        break
    }
  }

  push (obstacle, opts = { animationCounter: 60 }) {
    obstacle.__runtime.specialOpts = opts
    obstacle.__runtime.specialOpts.maxCounter = opts.animationCounter
    this.activeAnimations.push(obstacle)
  }

  reset () {
    this.activeAnimations.forEach(obstacle => {
      this.finishedAnimations.push(obstacle)
    })
    this.finishedAnimations.forEach(obstacle => {
      let sprite = obstacle.__runtime.graphic
      sprite.x = obstacle.x
      sprite.y = obstacle.y
      sprite.scale.x = obstacle.scale
      sprite.scale.y = obstacle.scale
      sprite.rotation = obstacle.rotation
      sprite.customData.layer = obstacle.layer
      sprite.alpha = 1
      obstacle.__runtime.touched = false
      sprite.customBounds = sprite.getBounds()
      sprite.customBounds.x += Global.camera.pivot.x - Global.screen.width / 2
      sprite.customBounds.y += Global.camera.pivot.y - Global.screen.height / 2
    })
    this.activeAnimations = []
    this.finishedAnimations = []
  }

  update () {
    for (let i = this.activeAnimations.length - 1; i >= 0; i--) {
      let obstacle = this.activeAnimations[i]
      if (obstacle.__runtime.specialOpts.animationCounter <= 0) {
        this.activeAnimations.splice(this.activeAnimations.indexOf(obstacle), 1)
        this.finishedAnimations.push(obstacle)
      } else {
        obstacle.__runtime.specialOpts.animationCounter -= 1
        this.updateObstacle(obstacle)
      }
    }
  }
}

export default new ItemPickupManager()
