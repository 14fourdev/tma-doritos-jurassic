import * as PIXI from 'pixi.js'
import Gyrosphere from '@/scripts/game/Gyrosphere'
import Global from '@/scripts/game/Global'

class EntityManager {
  constructor () {
    this.entities = []
    this.container = new PIXI.Container()
  }

  init () {
    this.player = new Gyrosphere()
    this.entities.push(this.player)
    this.container.addChild(this.player.sprite)
  }

  cull () {
    if (process.env.NODE_ENV === 'development') {
      if (Global.editor && Global.editor.enabled) {
        this.container.children.forEach(child => {
          if (child.customBounds) {
            child.visible = true
          }
        })
        return
      }
    }
    this.container.children.forEach(child => {
      if (child.customBounds) {
        child.visible = !(Global.camera.pivot.x + Global.screen.width / 2 < child.customBounds.x || Global.camera.pivot.x - Global.screen.width / 2 > child.customBounds.x + child.customBounds.width)
      }
    })
  }

  update () {
    this.entities.forEach(entity => entity.__update())
    // this.container.children.filter(child => child.filters).forEach(child => {
    //   child.filters[0].uniforms.uTime += 0.02
    // })
  }

  sort () {
    this.container.children.sort((a, b) => {
      return (a.customData.layer + (a.customData.priority || 0)) - (b.customData.layer + (b.customData.priority || 0))
    })
  }

  reset () {
    this.player.initDefaults()
  }
}

export default new EntityManager()
