// import LavaPitShader from '@/scripts/game/shaders/LavaPit.frag'

export default {
  dinobones: {
    type: 'obstacle',
    texture: 'obstacle-dinobones.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.5
  },
  lavapit: {
    type: 'obstacle',
    texture: 'obstacle-lavapit.png',
    // shader: LavaPitShader,
    anchor: {
      x: 0.5,
      y: 0.75
    },
    priority: -4,
    scale: 0.4
  },
  lavapit2: {
    type: 'obstacle',
    texture: 'obstacle-lavapit2.png',
    // shader: LavaPitShader,
    anchor: {
      x: 0.5,
      y: 0.75
    },
    priority: -4,
    scale: 0.4
  },
  lavapit3: {
    type: 'obstacle',
    texture: 'obstacle-lavapit3.png',
    // shader: LavaPitShader,
    anchor: {
      x: 0.5,
      y: 0.75
    },
    priority: -4,
    scale: 0.4
  },
  lavapit4: {
    type: 'obstacle',
    texture: 'obstacle-lavapit4.png',
    // shader: LavaPitShader,
    anchor: {
      x: 0.5,
      y: 0.75
    },
    priority: -4,
    scale: 0.4
  },
  oldgyro: {
    type: 'obstacle',
    texture: 'obstacle-oldgyro.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.4
  },
  rocks: {
    type: 'obstacle',
    texture: 'obstacle-rocks.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.4,
    bounds: {
      x1: 0.22,
      x2: 0.78
    }
  },
  rocks2: {
    type: 'obstacle',
    texture: 'obstacle-rocks2.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.4,
    bounds: {
      x1: 0.22,
      x2: 0.78
    }
  },
  rocks3: {
    type: 'obstacle',
    texture: 'obstacle-rocks3.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.4,
    bounds: {
      x1: 0.22,
      x2: 0.78
    }
  },
  rocks4: {
    type: 'obstacle',
    texture: 'obstacle-rocks4.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.4,
    bounds: {
      x1: 0.22,
      x2: 0.78
    }
  },
  rocks5: {
    type: 'obstacle',
    texture: 'obstacle-rocks5.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.4,
    bounds: {
      x1: 0.22,
      x2: 0.78
    }
  },
  impactcrater: {
    type: 'obstacle',
    texture: 'obstacle-impactcrater.png',
    anchor: {
      x: 0.48,
      y: 0.5
    },
    scale: 0.8,
    priority: -3,
    bounds: {
      x1: 0.43,
      x2: 0.5
    }
  },
  foliageclump1: {
    type: 'decoration',
    texture: 'decoration-foliageclump1.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  foliageclump2: {
    type: 'decoration',
    texture: 'decoration-foliageclump2.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  foliageclump3: {
    type: 'decoration',
    texture: 'decoration-foliageclump3.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  foliageclump4: {
    type: 'decoration',
    texture: 'decoration-foliageclump4.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  foliageclump5: {
    type: 'decoration',
    texture: 'decoration-foliageclump5.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  tree1: {
    type: 'decoration',
    texture: 'decoration-tree1.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  tree2: {
    type: 'decoration',
    texture: 'decoration-tree2.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  tree3: {
    type: 'decoration',
    texture: 'decoration-tree3.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  tree4: {
    type: 'decoration',
    texture: 'decoration-tree4.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  tree5: {
    type: 'decoration',
    texture: 'decoration-tree5.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  tree6: {
    type: 'decoration',
    texture: 'decoration-tree6.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  tree7: {
    type: 'decoration',
    texture: 'decoration-tree7.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  tree8: {
    type: 'decoration',
    texture: 'decoration-tree8.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  distantfern1: {
    type: 'decoration',
    texture: 'decoration-distantfern1.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  distantfern2: {
    type: 'decoration',
    texture: 'decoration-distantfern2.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1
  },
  dinoApatosaurus: {
    type: 'decoration',
    texture: 'decoration-dino-apatosaurus.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1,
    priority: 0.01
  },
  dinoBaryonyx: {
    type: 'decoration',
    texture: 'decoration-dino-baryonyx.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1,
    priority: 0.01
  },
  dinoBrachiosaurus: {
    type: 'decoration',
    texture: 'decoration-dino-brachiosaurus.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1,
    priority: 0.01
  },
  dinoCarnotaurus: {
    type: 'decoration',
    texture: 'decoration-dino-carnotaurus.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1,
    priority: 0.01
  },
  dinoGallimimus: {
    type: 'decoration',
    texture: 'decoration-dino-gallimimus.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1,
    priority: 0.01
  },
  dinoSinoceratops: {
    type: 'decoration',
    texture: 'decoration-dino-sinoceratops.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1,
    priority: 0.01
  },
  dinoTrex: {
    type: 'decoration',
    texture: 'decoration-dino-trex.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1,
    priority: 0.01
  },
  dinoTriceratops: {
    type: 'decoration',
    texture: 'decoration-dino-triceratops.png',
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 1,
    priority: 0.01
  },
  ramp: {
    type: 'obstacle',
    texture: 'obstacle-ramp.png',
    special: 'ramp',
    priority: 0.01,
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.5
  },
  powerup_boost: {
    type: 'obstacle',
    texture: 'powerup_boost.png',
    animations: {
      sheen: {
        rate: 3,
        frames: [
          'powerup_boost.png',
          'powerup_boost_0000.png',
          'powerup_boost_0001.png',
          'powerup_boost_0002.png',
          'powerup_boost_0003.png',
          'powerup_boost_0004.png',
          'powerup_boost_0005.png',
          'powerup_boost_0006.png'
        ]
      }
    },
    special: 'boost',
    priority: 0.05,
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.5
  },
  powerup_repair: {
    type: 'obstacle',
    texture: 'powerup_repair.png',
    animations: {
      sheen: {
        rate: 3,
        frames: [
          'powerup_repair.png',
          'powerup_repair_0000.png',
          'powerup_repair_0001.png',
          'powerup_repair_0002.png',
          'powerup_repair_0003.png',
          'powerup_repair_0004.png',
          'powerup_repair_0005.png',
          'powerup_repair_0006.png'
        ]
      }
    },
    special: 'repair',
    priority: 0.05,
    anchor: {
      x: 0.5,
      y: 0.85
    },
    scale: 0.5
  }
}
