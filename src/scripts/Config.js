import axios from 'axios'

class Config {
  constructor () {
    this.cache = {}
  }

  load (lang = 'en-us') {
    if (!this.cache[lang]) {
      this.cache[lang] = axios.get(`/assets/game/static/config/${lang}.json`).then(response => {
        this.config = response.data
      })
    }
    return this.cache[lang]
  }
}

export default new Config()
